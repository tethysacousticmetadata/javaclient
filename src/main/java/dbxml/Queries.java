package dbxml;

import java.lang.RuntimeException;
import java.lang.System;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

import java.io.IOException;
import java.io.StringReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathConstants;

import javax.ws.rs.core.MediaType;
import javax.xml.parsers.ParserConfigurationException;

// for pretty print
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.stream.StreamResult;

import dbxml.NamespaceContextMap;  // Namespace manager
import dbxml.Client;
import dbxml.JerseyClient;


public class Queries {

	/**
	 * Query interface
	 *
	 */

	/** 
	 * Types and data for mapping between ITIS taxonomic serial numbers
	 * and various representations
	 */

	enum SpeciesIdFormat {
		LATIN,  // Use Latin names, default
		TSN,  // Taxonomic serial numbers
		VERNACULAR,  // common language, e.g, English, French, Spanish
		ABBREV,  // Abbreviation map
	};

	private enum IO {
		INPUT,  // input format 
		OUTPUT; // output format
		
		public static final int size;
		static {
			size = values().length;
		}
	};
	
	public SpeciesIdFormat[] speciesFormat = new SpeciesIdFormat[IO.size];
	// which species abbreviation map when applicable
	public String[] speciesAbbrev = new String[IO.size];
	// which vernacular, e.g., English, when applicable
	public String[] speciesVernacular = new String[IO.size];
	
	
	// Handles communication with server
	public Client client;
	
	// builders for converting XML strings to document object models
	private DocumentBuilderFactory domFactory;
	private DocumentBuilder domBuilder;
	
	// namespace manager
	public NamespaceContextMap nsmap;
	
	// namespace for Tethys schemata
	public final String tethysNamespace = "http://tethys.sdsu.edu/schema/1.0";
	

	/***
	 * Queries - Construct a query handler. 
	 * @param client - Instance of Client interface for communicating with server,
	 *  (e.g. JerseyClient)
	 */
	public Queries(Client client) {
		
		// Set client - handles communication with server
		this.client = client;
		
		// Set default input and output species names to Latin
		Arrays.fill(speciesFormat, SpeciesIdFormat.LATIN);
		
		// Set up document object model builder
		this.domFactory = DocumentBuilderFactory.newInstance();
		this.domFactory.setNamespaceAware(true);
		try {
			this.domBuilder = this.domFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// We should not encounter a parser configuration
			// as the configuration is statically set...
			e.printStackTrace();
		}
		
		// Setup namespace manager
		this.nsmap = new NamespaceContextMap("ty", this.tethysNamespace);
		
	}

	/**
	 * getURL
	 * @return URL of the server 
	 */
	public java.net.URL getURL() {
		return client.getURL();
	}

	/**
	 * getURLString
	 * @return String representation of the server URL
	 */
	public String getURLString() {
		return client.getURL().toString();
	}

	/**
	 * Returns the version of the Java client as a string
	 * @return Java client library version string
	 */
	public static String getVersion() {
		String ver;
		
		// Load project properties file
		final Properties props = new Properties();
		try {
			props.load(Queries.class.getClassLoader().getResourceAsStream("project.properties"));
			ver = props.getProperty("version");
		} catch (IOException e) {
			ver = "Unable to acess project.properties file or find version within";
		}
		// Return version from properties
		return ver;
	}
	
	/**
	 * ping - Verify server is responding
	 * @return True if server is alive
	 */
	public boolean ping() {
		return client.ping();
	}
	
	/***
	 * resource- Get resource from Tethys server
	 * @param resourceId - Resource string relative to the web server, e.g. "Tethys/ping"
	 * @return String response
	 */
	public String resource(String resourceId) 
			throws UniformInterfaceException, ClientHandlerException {
		return client.get(resourceId);
	}
	
	/**
	 * Pretty print an XML string
	 * @param xmlstr - valid XML string
	 * @return indented XML string
	 */
	public String xmlpp(String xmlstr) {
		// pretty print XML
		try {
			Transformer serializer = SAXTransformerFactory.newInstance()
					.newTransformer();
			serializer.setOutputProperty(OutputKeys.INDENT, "yes");
			// serializer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,
			// "yes");
			serializer.setOutputProperty(
					"{http://xml.apache.org/xslt}indent-amount", "2");
			Source xmlSource = new SAXSource(new InputSource(
					new ByteArrayInputStream(xmlstr.getBytes())));
			StreamResult res = new StreamResult(new ByteArrayOutputStream());
			serializer.transform(xmlSource, res);
			return new String(
					((ByteArrayOutputStream) res.getOutputStream())
							.toByteArray());
		} catch (Exception e) {
			// TODO log error
			return xmlstr;
		}

	}

	/**
	 * Run an XQuery, returns XML
	 * @param query - XQuery string
	 * @return result string
	 * @throws Exception
	 */
	public String Query(String query) throws Exception {
		String result;
		try {
			result = client.query(query);
		} catch (Exception e) {
			abbreviateStack(e);
			throw e;
		}
			
		return result;
	}

	/**
	 * Run an XQuery, returns XML
	 * Prepends the XQuery with the Tethys default element namespace
	 * and lib: for Tethys XQuery library functions.  These are defined
	 * in lib/modules/Tethys.xq of the database directory.
	 * @param query
	 * @return
	 * @throws Exception
	 */
	public String QueryTethys(String query) throws Exception {
		String newquery = 
			"declare default element namespace \"http://tethys.sdsu.edu/schema/1.0\";\n" + 
			"import module namespace lib='http://tethys.sdsu.edu/XQueryFns' at 'Tethys.xq';\n" +
			query;
		
		return Query(newquery);
	}
	
	/**
	 * Public class for querying an XML database with a JSON dictionary.
	 *
	 * @param query - JSON specification, see Tethys RESTful Interface manual
	 *   for details.
	 * @param plan -
	 *    0 - execute query
	 *    1 - show query plan
	 *    2 - show XQuery
	 * @return XML result string
	 * @throws Exception
	 */
	public String QueryJSON(String query, int plan) throws Exception {

		String result;
		try {
			result = client.queryJSON(query, plan);
		} catch (Exception e) {
			abbreviateStack(e);
			throw e;
		}
			
		return result;
	}		
	
	/**
	 * Public class for querying an XML database with a simplified predicate
	 * structure that is transformed into an XQuery.  In general, users may 
	 * wish to call the more specific (and simpler) versions that retrieve
	 * specific types of information, e.g.:
	 *    getDetectionEffort
	 *    getDetections
	 *    getDeployments
	 *    getCalibrations
	 *    getLocalizationEffort
	 *    getLocalization
	 *    
	 * @param query - query string is a mini query language that allows one to specify
	 *    criteria for selection.  For example, to query all of the detections
	 *    for Zipheus cavirostris, one could specify the query:
	 *    
	 *    SpeciesId = "Ziphius cavirostris" and Deployment/Site = "M" and Project = "SOCAL" and Deployment/DeploymentId = 38
	 *    
	 *    a default set of values will be returned.  To specify a custom set of values to return,
	 *    follow the query with the word "return" and a list of elements to return.  For example
	 *    
	 *    SpeciesId = "Ziphius cavirostris" and Deployment/Site = "M" and Project = "SOCAL" and Deployment/DeploymentId = 38
	 *    return Detection/SpeciesId, Detection/Start, Detection/End
	 *    
	 *    When there is no ambiguity, a field name need not have the parent before the 
	 *    path separator (/).  For example, we wrote Project instead of Deployment/Project
	 *    (which would have also been acceptable).  
	 *     
	 * @param root - Document root name
	 *     This is the root element of the primary collection from which we are
	 *     querying.  As the example query above was a detection query, we would
	 *     use Detections.  The following table shows the root elements for Tethys
	 *     collections from which users are likely to query:
	 *     
	 *      Collection		Root
	 *      calibrations	Calibration
	 *      deployments		Deployment
	 *      detections		Detections
	 *      localizations	Localize
	 *      
	 * @param effort - Is this an effort query True|False
	 *      This field is only important when a user does not specify
	 *      a return list in the query.  Different default values are
	 *      provided for detections and localizations when effort is True|False
	 *      
	 * @param namespaces - Should namespaces be included in the returned XML?
	 *     A value of False strips all namespaces and may make processing easier
	 *     for users of the data that are not familiar with XML namespaces.
	 *     A value of True retains namespaces
	 *     
	 * @param enclose - When nested structures are returned, should they be enclosed in 
	 * 		an element?
	 *   	As an example, consider a list of detections that are to be returned.
	 *      If the user specifies Detection as the return element, all elements for
	 *      each detection will be returned.  However, the user may only be interested
	 *      in the Start and End time:
	 *          return Detection/Start, Detection/End
	 *      When False, a series of Start/End times will be returned one after the other
	 *      without an enclosing element:
	 *              Start 2024-05-01T12:00:00Z /Start
	 *              End   2024-05-01T12:00:05Z /End
	 *              Start 2024-05-01T12:01:00Z /Start
	 *              End   2024-05-01T12:01:05Z /End
	 *      In contrast, when True, they will be enclosed by their parent element (Detection)
	 *            Detection
	 *              Start 2024-05-01T12:00:00Z /Start
	 *              End   2024-05-01T12:00:05Z /End
	 *            /Detection
	 *            Detection
	 *              Start 2024-05-01T12:01:00Z /Start
	 *              End   2024-05-01T12:01:05Z /End'
	 *            /Detection                
	 *              
	 * @param plan
	 *    0 - execute query
	 *    1 - show query plan - produce XML document that indicates how the 
	 *        query will be parsed by the underlying database system 
	 *        (not useful for most users)
	 *    2 - show XQuery - translate query to XQuery, the underlying 
	 *        language used by the database
	 *    3 - show JSON query - translate query to a JSON query expression
	 * @return
	 * @throws Exception
	 */
	public String QuerySimple(String query, String root, boolean effort, 
			boolean namespaces, boolean enclose, int plan) throws Exception {
		
		JSONObject json = new JSONObject();
		
		// Configuration
		// Generate enclosing elements to help with structure?  
		// See Tethys Web Services Interface documentation for details
		json.put("root",  root);
		json.put("enclose", (enclose) ? 1 : 0);  
		// Resolve ambiguities assuming that this is an effort query?
		json.put("effort", (effort) ? 1 : 0);
		// Include XML namespaces in response? 
		json.put("namespaces", (namespaces) ? 1 : 0);
		
		// How should SpeciesId's be interpreted in query or in output?
		// Based on current settings
		addSpeciesMapping(json);
		
		json.put("query", query);
		
		String json_pkg = json.toJSONString();
		
		String result;
		if (plan == 3)
			result = json_pkg;
		else {
			// Let JSON query handler deal with this
			result = QueryJSON(json_pkg, plan);
		}

		return result;		
	}
	
	/**
	 * Execute a detection query.  
	 * @param query
	 *   Simple query language that allows conjunctions (and) of XML element conditions
	 *   and a comma separated list of items to return.  Disjunctions (or) are not allowed
	 *   due to restrictions on how they may be used, but one can generate an XQuery 
	 *   expression and edit if disjunctions are needed.
	 *   Sample query:
	 *
	 * 	 SpeciesId = "Ziphius cavirostris" and Deployment/Site = "M" and Project = "SOCAL" and Deployment/DeploymentId = 38
	 *   return Detection/SpeciesId, Detection/Start, Detection/End
	 *
	 * @param plan
	 *    0 - execute query (default if omitted)
	 *    1 - show query plan - produce XML document that indicates how the 
	 *        query will be parsed by the underlying database system 
	 *        (not useful for most users)
	 *    2 - show XQuery - translate query to XQuery, the underlying 
	 *        language used by the database
	 *    3 - show JSON query - translate query to a JSON query expression
	 *
	 * @return
	 * @throws Exception
	 */
	public String getDetections(String query, int plan) throws Exception {
		return QuerySimple(query, "Detections", false, false, true, plan);
	}
	
	public String getDetections(String query) throws Exception {
		return QuerySimple(query, "Detections", false, false, true, 0);
	}
	
	/**
	 * Execute a detection effort query.  
	 * @param query
	 *   Simple query language that allows conjunctions (and) of XML element conditions
	 *   and a comma separated list of items to return.  Disjunctions (or) are not allowed
	 *   due to restrictions on how they may be used, but one can generate an XQuery 
	 *   expression and edit if disjunctions are needed.
	 *   Sample query:
	 *
	 * 	 SpeciesId = "Ziphius cavirostris" and Deployment/Site = "M" and Project = "SOCAL" and Deployment/DeploymentId = 38
	 *
	 * @param plan
	 *    0 - execute query (default if omitted)
	 *    1 - show query plan - produce XML document that indicates how the 
	 *        query will be parsed by the underlying database system 
	 *        (not useful for most users)
	 *    2 - show XQuery - translate query to XQuery, the underlying 
	 *        language used by the database
	 *    3 - show JSON query - translate query to a JSON query expression
	 *
	 * @return
	 * @throws Exception
	 */
	public String getDetectionEffort(String query, int plan) throws Exception {
		return QuerySimple(query, "Detections", true, false, true, plan);
	}
	
	public String getDetectionEffort(String query) throws Exception {
		return QuerySimple(query, "Detections", true, false, true, 0);
	}
	
	/**
	 * Execute a localization query.  
	 * @param query
	 *   Simple query language that allows conjunctions (and) of XML element conditions
	 *   and a comma separated list of items to return.  Disjunctions (or) are not allowed
	 *   due to restrictions on how they may be used, but one can generate an XQuery 
	 *   expression and edit if disjunctions are needed.
	 *
	 * @param plan
	 *    0 - execute query (default if omitted)
	 *    1 - show query plan - produce XML document that indicates how the 
	 *        query will be parsed by the underlying database system 
	 *        (not useful for most users)
	 *    2 - show XQuery - translate query to XQuery, the underlying 
	 *        language used by the database
	 *    3 - show JSON query - translate query to a JSON query expression
	 *
	 * @return
	 * @throws Exception
	 */
	public String getLocalizations(String query, int plan) throws Exception {
		return QuerySimple(query, "Localize", false, false, true, plan);
	}
	
	public String getLocalizations(String query) throws Exception {
		return QuerySimple(query, "Localize", false, false, true, 0);
	}
	
	/**
	 * Execute a detection effort query.  
	 * @param query
	 *   Simple query language that allows conjunctions (and) of XML element conditions
	 *   and a comma separated list of items to return.  Disjunctions (or) are not allowed
	 *   due to restrictions on how they may be used, but one can generate an XQuery 
	 *   expression and edit if disjunctions are needed.
	 *   Sample query:
	 *
	 * @param plan
	 *    0 - execute query (default if omitted)
	 *    1 - show query plan - produce XML document that indicates how the 
	 *        query will be parsed by the underlying database system 
	 *        (not useful for most users)
	 *    2 - show XQuery - translate query to XQuery, the underlying 
	 *        language used by the database
	 *    3 - show JSON query - translate query to a JSON query expression
	 *
	 * @return
	 * @throws Exception
	 */
	public String getLocalizationEffort(String query, int plan) throws Exception {
		return QuerySimple(query, "Localize", true, false, true, plan);
	}
	
	public String getLocalizationEffort(String query) throws Exception {
		return QuerySimple(query, "Localize", true, false, true, 0);
	}

	/**
	 * Execute a deployment query
	 *
	 * @param query
	 *   Simple declarative query language that allows conjunctions (and) of XML element conditions
	 *   and a comma separated list of items to return.  Disjunctions (or) are not allowed
	 *   due to restrictions on how they may be used, but one can generate an XQuery 
	 *   expression and edit if disjunctions are needed.
	 *   Sample query:
	 *
	 *    	 
	 *
	 * @param plan
	 *    0 - execute query (default if omitted)
	 *    1 - show query plan - produce XML document that indicates how the 
	 *        query will be parsed by the underlying database system 
	 *        (not useful for most users)
	 *    2 - show XQuery - translate query to XQuery, the underlying 
	 *        language used by the database
	 *    3 - show JSON query - translate query to a JSON query expression
	 *
	 * @return
	 * @throws Exception	 * 
	 */
	public String getDeployments(String query, int plan) throws Exception  {
		return QuerySimple(query, "Deployment", true, false, true, plan);
	}
	
	public String getDeployments(String query) throws Exception {
		return QuerySimple(query, "Deployment", true, false, true, 0);
	}
	
	
	/**
	 * Execute a calibration query
	 *
	 * @param query
	 *   Simple declarative query language that allows conjunctions (and) of XML element conditions
	 *   and a comma separated list of items to return.  Disjunctions (or) are not allowed
	 *   due to restrictions on how they may be used, but one can generate an XQuery 
	 *   expression and edit if disjunctions are needed.
	 *
	 * @param plan
	 *    0 - execute query (default if omitted)
	 *    1 - show query plan - produce XML document that indicates how the 
	 *        query will be parsed by the underlying database system 
	 *        (not useful for most users)
	 *    2 - show XQuery - translate query to XQuery, the underlying 
	 *        language used by the database
	 *    3 - show JSON query - translate query to a JSON query expression
	 *
	 * @return
	 * @throws Exception	 * 
	 */
	public String getCalibration(String query, int plan) throws Exception  {
		return QuerySimple(query, "Calibration", true, false, true, plan);
	}
	
	public String getCalibration(String query) throws Exception {
		return QuerySimple(query, "Calibration", true, false, true, 0);
	}
	
	
	/**
	 * Execute a JSON query with default parameters (plan=0, enclose=1) 
	 * @param query
	 * @return
	 * @throws Exception
	 */
	public String QueryJSON(String query) throws Exception {
		return QueryJSON(query, 0);
	}
	
	/**
	 * Return a document
	 * @param collection - collection from which document should be retrieved
	 * @param docname - Name of document (frequently the filename used to
	 *     submit the document without extensions).
	 * @return XML document
	 * @throws Exception
	 */
	public String getDocument(String collection, String docname) throws Exception {
		String docquery = 
				String.format("for $d in collection(\"%s\") where base-uri($d) = \"dbxml:///%s/%s\" return $d",
						collection, collection,docname);
		return QueryTethys(docquery);
		
	}

	/**
	 * Return a CSV representation of a schema
	 * @param schema_root
	 * @return CSV string
	 */
	public String getSchema(String schema_root) throws Exception {
		return client.getSchema(schema_root);
	}
	
	/**
	 * Remove a document
	 * @param collection - collection from which document should be removed
	 * @param docname - Name of document
	 * @return
	 * @throws Exception
	 */
	public String removeDocument(String collection, String docname) throws Exception {
		return client.removeDocument(collection, docname);
	}
	// Show stack trace starting from a specified class and method
	// (Used to avoid lengthy stack traces in remote procedure calls)
	// method may be set to null to specify the last instance of a class
	private void abbreviateStack(Exception e, String className, String methodName) {
		// The exception stack is long, we only want to display
		// the exceptions from after 
		// Retrieve the exception stack, stack[0] is most recent
		StackTraceElement[] stack = e.getStackTrace();

		// Look for the first occurrence of specified class and method
		boolean found = false;
		int idx = 0;
		while (! found && idx < stack.length) {
			found = stack[idx].getClassName().equals(className);
			if (found && methodName != null) {
				found = stack[idx].getMethodName().equals(methodName);
			}
			if (! found) {
				idx = idx + 1;
			}
		}
		// Set the stack back trace to frames prior to and including
		// the one for which we searched (if we found them)
		if (found) {
			StackTraceElement[] finalstack;
			int elements = stack.length - idx;
			
			finalstack = new StackTraceElement[elements];
			System.arraycopy(stack, idx, finalstack, 0, elements);
			e.setStackTrace(finalstack);
		}
	}
	
	private void abbreviateStack(Exception e) {
		// Abbreviate the stack to the oldest called method

		// Retrieve the exception stack, stack[0] is most recent
		StackTraceElement[] stack = e.getStackTrace();
		StackTraceElement[] abbrev = new StackTraceElement[1];
		System.arraycopy(stack, stack.length - 1, abbrev, 0, 1);
		e.setStackTrace(abbrev);
	}
	
	public Document QueryReturnDoc(String query) throws Exception {
		String result = client.query(query);

		// Java interface expects an input source
		// to a parser which must be based on a streaming
		// input such as a reader
		StringReader reader = new StringReader(result);
		InputSource isource = new InputSource(reader);

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		Document doc = null;

		// XML parser will discard extraneous whitespace
		factory.setIgnoringElementContentWhitespace(true);
		
		try {
			builder = factory.newDocumentBuilder();
			try {
				doc = builder.parse(isource);
			} catch (org.xml.sax.SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (ParserConfigurationException e1) {
			e1.printStackTrace();
		}

		return doc;
	}
	
	/**
	 * getDocumentObjectModel
	 * @param xmlstr - string representation of XML
	 * @return Document - document object model
	 */
	public Document getDocumentObjectModel(String xmlstr) {
	    // build document object model
	    InputSource xmlsrc = new InputSource(new StringReader(xmlstr));
	    Document dom = null;
		try {
			dom = this.domBuilder.parse(xmlsrc);
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return dom;
	}
	
	/**
	 * Build an XPath expression that can be evaluated 
	 * @param query - XPath 1.0 query
	 * @return XPathExpression
	 * @throws XPathExpressionException 
	 */
	private XPathExpression buildXPath(String query) throws XPathExpressionException {
		// Construct the XPath evaluator
	    XPathFactory xpathfactory = XPathFactory.newInstance();
	    XPath xpath = xpathfactory.newXPath();
	    xpath.setNamespaceContext(this.nsmap);  // enable namespaces, e.g., ty:
	    // Validate query syntax
	    XPathExpression xpathExpression = xpath.compile(query);
	    
	    // Return expression that can be evaluated on a Document
	    return xpathExpression;
	}

	/**
	 * Given an XML document as a document object model (DOM), execute the XPath 1.0 query
	 * @param xmldom - DOM representation of XML document
	 * @param query - XPath query
	 * @return 
	 * @throws XPathExpressionException - Bad XPath syntax
	 */
	public String XPath(Document xmldom, String query) throws XPathExpressionException {
		
		// prepare the query
	    XPathExpression xpathExpression = buildXPath(query);    
    
	    // Execute the query
	    String result = (String) xpathExpression.evaluate(xmldom, XPathConstants.STRING);
	    
	    return result;	    
	}
	
	/**
	 * Given an XML document, execute the XPath 1.0 query
	 * @param xmlstr - XML document string
	 * @param query - XPath query
	 * @return 
	 * @throws XPathExpressionException - Bad XPath syntax
	 */
	public String XPath(String xmlstr, String query) throws XPathExpressionException {

		// prepare the query
	    XPathExpression xpathExpression = buildXPath(query);    

	    // Convert XML to document object model
	    Document dom = getDocumentObjectModel(xmlstr);
	    
	    // Execute the query
	    String result = (String) xpathExpression.evaluate(dom, XPathConstants.STRING);
	    
	    return result;
	    
	}

	/**
	 * SpeciesId format types  
	 * @param type
	 *    must be:  TSN - taxonomic serial number
	 *              Latin - use Latin species names
	 *              Vernacular - Use named vernacular, e.g. Engish
	 *              Abbrev - Use named abbreviation map
	 * @param value - Vernacular or Abbrev string (omit otherwise)
	 * Species are always encoded as ITIS taxonomic serial numbers
	 * https://itis.gov
	 * setSpeciesId* allows setting how users will input or output the 
	 * species identifiers.  
	 * getSpeciesId* reports the current settings for
	 *   Input, Output, or both InputOutput 
	 * 
	 */
	public void setSpeciesIdOutput(String type, String value) {
		setSpeciesIdStr(IO.OUTPUT, type, value);
	}
	
	public void setSpeciesIdOutput(String type) {
		setSpeciesIdStr(IO.OUTPUT, type, null);
	}
	
	public void setSpeciesIdInput(String type, String value) {
		setSpeciesIdStr(IO.INPUT, type, value);
	};
	
	public void setSpeciesIdInput(String type) {
		setSpeciesIdStr(IO.INPUT, type, null);
	};
	
	public void setSpeciesIdInputOutput(String type, String value) {
		setSpeciesIdStr(IO.INPUT, type, value);
		setSpeciesIdStr(IO.OUTPUT, type, value);
	};
	
	public void setSpeciesIdInputOutput(String type) {
		setSpeciesIdStr(IO.INPUT, type, null);
		setSpeciesIdStr(IO.OUTPUT, type, null);
	};
	
	public String getSpeciesIdOutput() {
		return getSpeciesIdStr(IO.OUTPUT);
	};
	
	public String getSpeciesIdInput() {
		return getSpeciesIdStr(IO.INPUT);
	};
	
	public String getSpeciesIdInputOutput() {
		return "Input: " + getSpeciesIdStr(IO.INPUT) +
				" Output: " + getSpeciesIdStr(IO.OUTPUT);
	}
	
	/**
	 * Retrieve current species encoding
	 * @param type INPUT or OUTPUT encoding
	 * @return Latin, TSN, vernacular, or abbreviation
	 */
	private String getSpeciesIdStr(IO type) {
		String value = new String();
		int index = type.ordinal();
		SpeciesIdFormat format = speciesFormat[index];
		switch (format) {
		case LATIN:
			value = "Latin";
			break;
		case TSN:
			value = "TSN";
			break;
		case VERNACULAR:
			value = speciesVernacular[index];
			break;
		case ABBREV:
			value = speciesAbbrev[index];
			break;
		}
		return value;
	}
	
	/*
	 * Functions for generating settings to translate between TSNs and Latin, Vernacular, and Abbreviations
	 */
	
	/**
	 * 
	 * @return - Tethys template for TSN to output format
	 */
	public String tsnDecode() {
		String decode;
		int index = IO.OUTPUT.ordinal();
		
		switch(speciesFormat[index]) {
		case TSN:
			decode = "%s";  // no translation
			break;
		case ABBREV:
			// Tethys XQ lib TSN -> species abbreviation
			decode = "lib:tsn2abbrev(%s, \"" + speciesAbbrev[index] + "\")";
			break;
		case VERNACULAR:
			decode = "lib:tsn2vernacular(%s, \"" + speciesVernacular[index] + "\")";
			break;
		case LATIN:
		default:
			// Tethys XQ lib TSN -> Latin
			decode = "lib:tsn2completename(%s)";
			break;		}
		return decode;
	}
	
	/**
	 * @return - Tethys template for input format to TSN
	 */
	public String tsnEncode() {
		String encode;
		int index = IO.INPUT.ordinal();
		
		switch (speciesFormat[index]) {
		case TSN:
			// identity map
			encode = "%f";
			break;
		case ABBREV:
			// Abbreviation -> TSN
			encode = "lib:abbrev2tsn(\"%tsn\", \"" + speciesAbbrev[index] + "\")";
			break;
		case VERNACULAR:
			// Vernacular -> TSN
			encode = "lib:vernacular2tsn(\"%s\", \"" + speciesVernacular[index] + "\")";
			break;
		default:
		case LATIN:
			// Latin -> TSN
			encode = "lib:completename2tsn(\"%s\")";
			break;
		}
		return encode;
	}

	/**
	 * Set species encoding
	 * @param type - INPUT or OUTPUT encoding
	 * @param format - Latin, TSN, Abbrev, or Vernacular
	 * @param arg - For Abbrev and Vernacular, which abbreviation
	 *    map or vernacular should be used, e.g. English 
	 */
	private void setSpeciesIdStr(IO type, String format, String arg) throws RuntimeException {
		int index = type.ordinal();
		switch (format) {
		case "Latin":
			speciesFormat[index] = SpeciesIdFormat.LATIN;
			break;
		case "TSN":
			speciesFormat[index] = SpeciesIdFormat.TSN;
			break;
		case "Abbrev":
			speciesFormat[index] = SpeciesIdFormat.ABBREV;
			speciesAbbrev[index] = arg;
			break;
		case "Vernacular":
			speciesFormat[index] = SpeciesIdFormat.VERNACULAR;
			speciesVernacular[index] = arg; 
			break;
		default:
			throw new RuntimeException(
					"Bad format specification, must be in " +
					"{Latin, TSN, Abbrev, Vernacular}");
		}
	}
	
	/**
	 * Given a JSON object, add a subkey (if needed) 
	 * that indicates how species input and output are
	 * to be encoded.
	 * @param json - existing JSONObject, will have "species"
	 *    key added if we need to translate to/from 
	 *    taxonomic serial numbers.
	 */
	private void addSpeciesMapping(JSONObject json) {
		JSONObject species = new JSONObject();
		
		JSONArray operands = null; 
		
		/*
		 *  Handle input encoding
		 */
		int idx = IO.INPUT.ordinal();
		String op = null;
		// Determine operation and operands
		switch (speciesFormat[idx]) {
			case TSN:
				break;  // do nothing
			case LATIN:
				op = "lib:completename2tsn";
				operands = new JSONArray();
				operands.add("%s");
				break;
			case VERNACULAR:
				op = "lib:vernacular2tsn";
				operands = new JSONArray();
				operands.add("%s");
				operands.add(this.speciesVernacular[idx]);
				break;
			case ABBREV:
				op = "lib:abbrev2tsn";
				operands = new JSONArray();
				operands.add("%s");
				operands.add(this.speciesAbbrev[idx]);
				break;
		}
		// Populate JSON
		if (op != null) {
			
			JSONObject query = new JSONObject();
			query.put("op", op);
			query.put("optype", "function");
			if (operands != null) 
				query.put("operands",  operands);			
			
			species.put("query", query);
		}
		
		
		/*
		 * Handle output encoding
		 */
		idx = IO.OUTPUT.ordinal();
		op = null;
		// Determine operation and operands
		switch (speciesFormat[idx]) {
			case TSN:
				break;  // do nothing
			case LATIN:
				op = "lib:tsn2completename";
				break;
			case VERNACULAR:
				op = "lib:tsn2vernacular";				operands = new JSONArray();
				operands.add("%s");
				operands.add(this.speciesVernacular[idx]);
				break;
			case ABBREV:
				op = "lib:tsn2abbrev";
				operands = new JSONArray();
				operands.add("%s");
				operands.add(this.speciesAbbrev[idx]);
				break;
		}
		// Populate 
		if (op != null) {
			JSONObject result = new JSONObject();
			result.put("op", op);
			result.put("optype", "function");
			if (operands != null) 
				result.put("operands",  operands);
			species.put("return", result);
		}
		
		if (species.size() > 0) {
			// Some translation is being done, add encoding
			json.put("species",  species);
		}
	}
	

}
