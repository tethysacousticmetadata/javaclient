package dbxml;

import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.TrustManager;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.GZIPContentEncodingFilter;
import com.sun.jersey.api.representation.Form;
import com.sun.jersey.multipart.FormDataMultiPart;




public class JerseyClient implements dbxml.Client {

	// Resources provided by Tethys
	private WebResource tethysService = null;
	private WebResource XQueryResource = null;
	private URL url;
	// Validate SSL certificates?
	// Setting to false makes us vulnerable to "man in the middle"
	// attacks but permits self signed certificates which cost
	// nothing.
	private boolean CertificateValidation = true;

	public JerseyClient(URL siteurl) {
		commonInit(siteurl);
	}
	
	public JerseyClient(String siteurl) { 
		URL url;
		// set up the URL
		try {
			url = new URL(siteurl);
		} catch (MalformedURLException e) {
			throw new RuntimeException("bad URL", e);
		}
		commonInit(url);
	}
	

	/***
	 * ping - Verify Tethys server is communicating with client.
	 * @return
	 */
	public boolean ping() {
		boolean alive;
		
		try {
			String response = get("Tethys/ping");
			if (response.contains("alive")) {
				alive = true;
			} else {
				alive = false;
			}			
		} catch (UniformInterfaceException e) {
			alive = false;
		} catch (ClientHandlerException e) {
			alive = false;
		}
		return alive;
	}
	
	
	/***
	 * get- Get resource from specified resource on Tethys server
	 * @param resourceId - Resource string relative to the web server, e.g. "Tethys/ping"
	 * @return String response
	 */
	public String get(String resourceId) 
			throws UniformInterfaceException, ClientHandlerException 
	{
		WebResource resource = tethysService.path(resourceId);
		
		String response = resource.get(String.class);

		return response;
	}
	
	/***
	 * Execute an XQuery
	 * @param xquery - XML XQuery to execute
	 * @return XML result
	 */
	public String query(String xquery) {
		
		// Set up query
		FormDataMultiPart formdata = new FormDataMultiPart();
		
		formdata.field("XQuery", xquery);

		String response = null;
		try {
			response = XQueryResource.type(MediaType.MULTIPART_FORM_DATA)
					.accept(MediaType.APPLICATION_XML)
					.header("Accept-Encoding", "gzip")
					.post(String.class, formdata);
		} catch (UniformInterfaceException e) {
			ClientResponse r;
			r = e.getResponse();
			String msg = r.getEntity(String.class);
			if (msg.isEmpty())
				msg = e.getMessage();
			throw new RuntimeException(msg);
		}
				
		return response;
	}
	
	/***
	 * @param json - JSON query specification, see Tethys Restful Web
	 *        Interfaces document for details on specification.
	 * @param plan -  
	 *    0 : Run query
	 *    1 : Return dbXML schema plan (does not execute query)
	 *    2 : Return XQuery (does not execute query)
	 * @return query result, plan, or XQuery
	 */
	public String queryJSON(String json, int plan) {

		// Set up query
		FormDataMultiPart formdata = new FormDataMultiPart();
		
		// JSON specification still goes in XQuery field
		formdata.field("JSON", json);
		formdata.field("plan", String.valueOf(plan));

		String response = null;
		try {
			response = XQueryResource.type(MediaType.MULTIPART_FORM_DATA)
					.accept(MediaType.APPLICATION_XML)
					.header("Accept-Encoding", "gzip")
					.post(String.class, formdata);
		} catch (UniformInterfaceException e) {
			ClientResponse r;
			r = e.getResponse();
			String msg = r.getEntity(String.class);
			if (msg.isEmpty())
				msg = e.getMessage();
			throw new RuntimeException(msg);
		}
				
		return response;
	}
	
	/**
	 * @param json - JSON query specification, see Tethys Restful Web
	 *        Interfaces document for details on specification.
	 * @return query result
	 */
	public String queryJSON(String json) {
		return queryJSON(json, 0);
	}
	
	/**
	 * 
	 * removeDocument
	 * @param collection  - Collection from which to delete
	 * @param docId - document id.  This is the last part of the uri
	 * 
	 * e.g. uri:  dbxml:///Detections/SOCAL32M_mysticete
	 * delete_docId("Detections", "SOCAL32M_mysticete")
	 * 				
	 * @return operation status as XML string
	 */
	public String removeDocument(String collection, String docId) {
		//TODO fix the input for .delete(), change to apache
		WebResource collectionResource = tethysService.path(collection);
		WebResource remove = collectionResource.queryParam("DocId", docId);
		//Form formdata = new Form();
		//formdata.add("DocId", docId);
		
		String response = null;
		try {
			response = remove
				.accept(MediaType.APPLICATION_XML)
				.delete(String.class); //, formdata);
		} catch (UniformInterfaceException e) {
			ClientResponse r;
			r = e.getResponse();
			String msg = r.getEntity(String.class);
			if (msg.isEmpty())
				msg = e.getMessage();
			throw new RuntimeException(msg);
		}
		
		return response;
	}
	
	private void commonInit(URL siteurl) {
		
		if (! CertificateValidation) {
			TrustManagerOps.disableCertificateValidation();
		}
		url = siteurl;
		//ClientConfig config = new DefaultClientConfig();
		com.sun.jersey.api.client.Client client = 
				com.sun.jersey.api.client.Client.create();
		client.addFilter(new GZIPContentEncodingFilter(false)); //we may need this to deflate gzipped
		// responses
		tethysService = client.resource(siteurl.toString());
		XQueryResource = tethysService.path("XQuery");
		
	}
	
	public static void main(String [] argv) {
		@SuppressWarnings("unused")
		JerseyClient test = new JerseyClient("http://localhost:8000");
		@SuppressWarnings("unused")
		boolean x = true;
	}
	
	/**
	 * Retrieve CSV representation of a schema
	 * @param schema_root - Root element of the schema
	 * @return CSV string
	 */
	public String getSchema(String schema_root) {
		MultivaluedMap query_params = new MultivaluedHashMap();
		query_params.add("format", "CSV");  // Return format
		WebResource schema = tethysService.path("Schema")
				.path(schema_root)
				.queryParams(query_params);
		String result = schema.accept(MediaType.TEXT_PLAIN)
				.get(String.class);
		return result;
	}
	
	public URL getURL() {
		return url;
	}	
	
	public boolean getCertificateValidation() {
		return CertificateValidation; 
	}
	
	public void setCertificateValidation(boolean validate) {
		CertificateValidation = validate;
	}
	
	
}
