package dbxml.uploader;

import java.io.File;

public class Util {

	/****
	 * Strip extension from a filename
	 * @param s
	 * @return
	 */
	
	public final static String stripExt(String s) {
		int pos = s.lastIndexOf('.');
		if (pos > 0)
			return s.substring(0, pos);
		else
			return s;
	}
	
	public final static String stripExt(File f) {
		return stripExt(f.getName());
	}
	
	public final static String baseName(String s) {
		// Find last separator
		int sepPos = s.lastIndexOf(File.separatorChar);
		if (sepPos > 0)
			s = s.substring(sepPos+1);
		return stripExt(s);	
	}
	
	public final static String baseName(File f) {
		return baseName(f.getName());
	}
}
