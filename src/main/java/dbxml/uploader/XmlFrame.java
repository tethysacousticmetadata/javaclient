package dbxml.uploader;



import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;

import javax.swing.JScrollPane;

import net.miginfocom.swing.MigLayout;

import javax.swing.JTextPane;
import javax.swing.JTextArea;



class XmlFrame extends JFrame{
	
	private static final long serialVersionUID = 88L;
	private JPanel contentPane;
	
	
	public XmlFrame(SpecWriter writer, Rectangle bounds) {
		//set intial features
		//TODO: base position on ImportFrame
		setTitle("Import Specification (XML)");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(bounds);
		// Set icon if we can find it.
		URL iconUrl = getClass().getClassLoader().getResource("Tethys.png");
		if (iconUrl != null) { 
			ImageIcon icon = new ImageIcon(iconUrl);
			if (icon != null)
				this.setIconImage(icon.getImage());
		}
		//build components
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("fill,insets 0 ", (String) null, (String) null));
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, "cell 0 0,alignx left,aligny top,grow");
		
		JTextPane textPane = new JTextPane();
		textPane.setEditable(false);
		scrollPane.setViewportView(textPane);
		//String evt = ((Boolean) javax.swing.SwingUtilities.isEventDispatchThread()).toString();
		try {
			textPane.setText(writer.writeSpec());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Window displaying XML Specification
	 * TODO: Color coding,
	 *
	 */
	

	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			public void run() {
				XmlFrame frame = new XmlFrame(new SpecWriter(),new Rectangle(500,100,400,300));
				frame.setVisible(true);
			}
		});
	}
}