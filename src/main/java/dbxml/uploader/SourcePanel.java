package dbxml.uploader;	

import java.awt.Dimension;
import java.awt.event.*;
import java.io.File;
import java.net.URL;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import net.miginfocom.swing.MigLayout;


public class SourcePanel extends JPanel{
	
		final String[] labels = {"Path to File:",
								 "ODBC:",
								 "MySQL:",
								 "MSAccess:",
								 };

		private static final long serialVersionUID = 1L;
		JTextField sourceString;
		private JTextField sourceName;
		//TODO: workingDir set?
		String workingDir = null;
		File targetFile;
		JButton btnDelete;
		JTextField txtUserName;
		JPasswordField txtPassword;
		private JButton btnBrowse;
		private JLabel lblPath;
		private JLabel lblLogin;
		enum SourceTypes {
				FILE,
				RESOURCE,				
			};
		SourceTypes type;
		String fname="";
		

		private JLabel lblName;
		
		/**
		 * Constructor
		 * @param cBoxIndex
		 */
		
		public SourcePanel(int cBoxIndex){

			//TODO migrate components to a createMethod
			//create a text field for file or connection string
			sourceString = new JTextField();
			sourceString.setToolTipText("The file path or connection string");
			
			//if its a connection string, do some stuff with login fields
			if(cBoxIndex != 0){
				setType(SourceTypes.RESOURCE);
				//add a listener for the text
				sourceString.getDocument().addDocumentListener(new ODBCListener(this));
				//add the login property, default to false
				this.putClientProperty("login", false);
			}else{
				setType(SourceTypes.FILE);
			}
			
			
			/*removed drag n drop, should add sources not overwrite filepaths
			sourceString.setDropTarget(new DropTarget(){
			    public synchronized void drop(DropTargetDropEvent evt) {
			        try {
			            evt.acceptDrop(DnDConstants.ACTION_COPY);
			            @SuppressWarnings("unchecked")
						List<File> droppedFiles = (List<File>)
			                evt.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
			            for (File file : droppedFiles) {
			            	sourceString.setText(file.getPath());
			            }
			        } catch (java.awt.datatransfer.UnsupportedFlavorException e){
			        	JFrame frame = new JFrame();
			        	JOptionPane.showMessageDialog(frame, "Unrecognized file type\nPlease extract archived files.","Drag n Drop Error", JOptionPane.ERROR_MESSAGE);
			        } catch (Exception ex) {
			            ex.printStackTrace();
			        }
			    }
			});
			*/
			sourceString.setColumns(50);
			
			
			setLayout(new MigLayout("fillx, gap 0 0,insets 4", "[96px][250.00px,grow,fill]4[2]12[right]", "[]4[]"));
			
			//create a Browse button for File types
			btnBrowse = new JButton("...");
			btnBrowse.setMaximumSize(new Dimension(30, 20));
			btnBrowse.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					// todo - allow one to call Import specifying a directory
					// add to constructor
					JFileChooser chooser;
					if (workingDir != null)
						chooser = new JFileChooser(workingDir);
					else
						chooser = new JFileChooser(workingDir);

					// Specify known extensions - last one goes first
					
					// XML
					String[] xml_ext = {"xml"};
					DialogFileFilter xml_flt = new DialogFileFilter(xml_ext,
							"eXtensible Markup Language");
					chooser.addChoosableFileFilter(xml_flt);
					// Workbook/CSV
					String[] spreadsheet_ext = { "xls", "xlsx", "csv" };
					DialogFileFilter spreadsheet_flt = 
							new DialogFileFilter(spreadsheet_ext,
									"Workbook/comma separated values");
					chooser.addChoosableFileFilter(spreadsheet_flt);
					
					//Access files
					String[] access_ext = {"accdb","mdb"};
					DialogFileFilter access_flt = new DialogFileFilter(access_ext,
							"Microsoft Access");
					chooser.addChoosableFileFilter(access_flt);		
					chooser.setFileHidingEnabled(false);
					chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
					chooser.setMultiSelectionEnabled(false);
					chooser.setDialogType(JFileChooser.OPEN_DIALOG);
					chooser.setDialogTitle("Select document to import");

					int result = chooser.showOpenDialog(SourcePanel.this);
					if (result == JFileChooser.APPROVE_OPTION) {
						targetFile = chooser.getSelectedFile();
						setText(targetFile.toString());
						setFileName(targetFile.getName());
					}
				}
			});
			add(btnBrowse, "cell 2 1,alignx left,aligny center");
			if(this.getType() != SourceTypes.FILE){
				btnBrowse.setVisible(false);
			}
			
			
			
			
			
			
			//set label text depending on type. 0 = Path. Else = connection string.
			String labelText = labels[cBoxIndex];
			
			lblName = new JLabel("Source Name:");
			lblName.setToolTipText("Should reflect the @source attribute in the SourceMap");
			add(lblName, "cell 0 0");
			
			sourceName = new JTextField();
			sourceName.setToolTipText("Name of this source");
			add(sourceName, "cell 1 0,alignx center,aligny center");
			sourceName.setColumns(10);

			lblPath = new JLabel(labelText);
			add(lblPath, "cell 0 1,alignx leading");
			add(sourceString, "cell 1 1,alignx center,aligny center");
			
			btnDelete = new JButton();
			btnDelete.setMaximumSize(new Dimension(20,20));
			URL iconUrl = getClass().getClassLoader().getResource("x.png");
			if (iconUrl != null) { 
				ImageIcon icon = new ImageIcon(iconUrl);
				if (icon != null)
					btnDelete.setIcon(icon);
			}
			add(btnDelete, "cell 3 1,alignx right,aligny center");
			
			if (cBoxIndex != 0){
			//ODBC name/password fields. hide unless <*Password*> etc in field
			txtUserName = new JTextField();
			txtUserName.setVisible(false);
			
			lblLogin = new JLabel("Credentials:");
			lblLogin.setVisible(false);
			add(lblLogin, "hidemode 3,cell 0 2,alignx left");
			txtUserName.setText("Username");
			add(txtUserName, "flowx,hidemode 3,cell 1 2,growx 50");
			txtUserName.setColumns(10);
			
			txtPassword = new JPasswordField();
			txtPassword.setVisible(false);
			txtPassword.setToolTipText("Password");
			txtPassword.setText("Password");
			add(txtPassword, "hidemode 3,cell 1 2,growx 50");
			txtPassword.setColumns(10);
			}
		}
		
		//Sets and Gets
		
		/**
		 * Wrapper for the sourceString field
		 * @param text - text to display, like file path
		 */
		public void setText(String text){
			this.sourceString.setText(text);
		}
		
		public void setType(SourceTypes type){
			this.type = type;
		}
		
		File getFile(){
			return this.targetFile;
		}
		
		void setFile(File file){
			this.targetFile = file;
		}
		
		public SourceTypes getType(){
			return this.type;
		}
		/**
		 * Gets the name of the source
		 * 
		 */
		public String getSourceName(){
			return this.sourceName.getText();
		}
		/**
		 * for file types, gets the filename
		 * @return
		 */
		public String getFileName() {
			return this.fname;
		}
		String getBasename(){
			String basename = this.fname.replaceFirst("[.][^.]+$", "");
			return basename;
		}
		
		public void setFileName(String fname){
			this.fname = fname;
		}
		
		public String getConnectionString(){
			if (this.type == SourceTypes.RESOURCE){
				return this.sourceString.getText();
			}else
				return null;
		}
		
		public String getFilePath(){
			String path = this.sourceString.getText();
			if (this.type == SourceTypes.FILE && !path.isEmpty()){
				return path;
			}else
				return null;
		}
		
		/**
		 * Does the 'initialization' of the panel once a file is dropped
		 * 
		 * @param file
		 */
		void setDropped(File file){
			this.setText(file.getPath());
			this.setFile(file);
			this.setFileName(file.getName());
		}
		
		/**
		 * Decides how to display the login fields
		 * @param state - 0:show none, 1:username, 2:pw, 3:both
		 */
		public void reveal(int state){
			
			switch(state){
				case 0: this.txtPassword.setVisible(false);
						this.txtUserName.setVisible(false);
						this.lblLogin.setText("Credentials:");
						this.lblLogin.setVisible(false);
						return;
				case 1: this.txtUserName.setVisible(true);
						this.txtPassword.setVisible(false);
						this.lblLogin.setText("Credentials:");
						break;
				case 2:	this.txtPassword.setVisible(true);
						this.txtUserName.setVisible(false);
						this.lblLogin.setText("Password:");
						break;
				case 3: this.txtUserName.setVisible(true);
						this.txtPassword.setVisible(true);
						this.lblLogin.setText("Credentials:");
						break;
			}
			
			this.lblLogin.setVisible(true);
			
			
			this.revalidate();
			this.repaint();
		}
		
		
		//Create a class for DocumentListening. Will show the login fields if <*Password*> or <*UserName*> is written
		class ODBCListener implements DocumentListener{
			final String pw = "<*Password*>";
			final String usr = "<*UserName*>";
			final String srvr = "<*ServerName*>";
			final JPanel spanel;
			
			public ODBCListener(SourcePanel panel){
				this.spanel = panel;
			}
			
			public void insertUpdate(DocumentEvent e){
				//character was typed
				getStringState(e);
			}
			public void removeUpdate(DocumentEvent e){
				//character was removed
				getStringState(e);
			}
			public void changedUpdate(DocumentEvent e){
				//only works on non-plain text components
				
			}
			public void getStringState(DocumentEvent e){
				//see if the string contains user/password
				//if it does, we change the "login" property,
				//listener of the ImportFrame should pick this up, and then
				//show the login fields
				//TODO: this shouldn't keep putting properties over and over.
				if(sourceString.getText().contains(pw) && sourceString.getText().contains(usr)){
					spanel.putClientProperty("login", 3);
				}else if (sourceString.getText().contains(pw) && !sourceString.getText().contains(usr)){
					spanel.putClientProperty("login", 2);
				}else if(!sourceString.getText().contains(pw) && sourceString.getText().contains(usr)){
					spanel.putClientProperty("login", 1);
				}else
					spanel.putClientProperty("login", 0);
				
			}


		}




	}