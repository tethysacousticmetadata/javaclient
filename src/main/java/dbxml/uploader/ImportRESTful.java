package dbxml.uploader;

import java.lang.StringBuilder;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.String;
import java.lang.RuntimeException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.security.GeneralSecurityException;

// import java.nio.file.Paths;
// import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.zip.Deflater;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.httpclient.contrib.ssl.EasySSLProtocolSocketFactory;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.text.StringEscapeUtils;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import javax.xml.xpath.*;
import javax.swing.SwingUtilities;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;



//model:  http://harryjoy.com/2012/09/08/simple-rest-client-in-java/

// Add-on packages
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.filter.ConnectionListenerFilter;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.MultiPartMediaTypes;
import com.sun.jersey.multipart.file.FileDataBodyPart;
import com.sun.jersey.client.apache.ApacheHttpClient;
import com.sun.jersey.client.apache.config.DefaultApacheHttpClientConfig;

import dbxml.uploader.ProgressMonitor.State;






// Notes about using Jersey
//
// Standard HTTP client appears to have some bugs with chunked
// transmission, see conversation between Martin Matula
// (Jersey developer) and developer "Eric" in stackoverflow:
// http://stackoverflow.com/questions/11176824/preventing-the-jersey-client-from-causing-an-outofmemory-error-when-posting-larg
//
//  


public class ImportRESTful {
	private ClientConfig config;
	private Client client;
	private WebResource resourceRoot;
	private WebResource resourceCollection;
	private UploadWorker guilog;
	private Writer log;
	private DefaultMediaTypePredictor predictor;
	private String spec;
	private ImportSpecification importspec;
	private FileSystem filesys;
	
	private int KB = 1024;  // kilobyte
	private int MB = KB^2;  // megabyte
	private int chunkSize = 100*KB;  // read bytes in blocks of this size
	private int largefile = MB; 
	private final int BAD_REQUEST = 400;  // Bad http request code 

	
	// Translation maps
	String importMap = null;
	String speciesAbbreviationMap = null;
	
	// Validate SSL certificates?
	// Setting to false makes us vulnerable to "man in the middle"
	// attacks but permits self signed certificates which cost
	// nothing.
	private boolean CertificateValidation = false;	
	
	public ImportRESTful(Writer log, boolean chunked) {
		this.log = log;

		
		
		// Transmit in chunks rather than reading everything into the heap
		// and then sending. for some reason, chunked encoding does not allow
		// formvar processing, but bodyparts are OK
		config = new DefaultApacheHttpClientConfig();
		if(chunked)
			config.getProperties().put(
					DefaultApacheHttpClientConfig.PROPERTY_CHUNKED_ENCODING_SIZE, 0);
		
		client = ApacheHttpClient.create(config);

		// Details on what should be imported into the database
		// TODO:  We have an older version of this that is used only
		// on the GUI path.  We'll need to factor it out.
		importspec = new ImportSpecification();
		
		predictor =	DefaultMediaTypePredictor.getInstance();
		resourceRoot = null;
		
		// Construct the filesystem object for resolving paths etc.
		filesys = FileSystems.getDefault();
	}


	
	/**
	 * setURI - Set Tethys server address
	 * @param url
	 * @return - false if unable to set, log message will be written if possible
	 */
	@SuppressWarnings("deprecation")
	public boolean setURI(URL url) {
		URI uri = null;
		boolean status = true;
		try {
			uri = url.toURI();
			setURI(uri);
		} catch (URISyntaxException e) {
			write(e.toString());
			status = false;
		}
		

		return status;
	}
	
	/**
	 * setURI - Set Tethys server address
	 * @param uriString
	 * @return - false if unable to set, log message will be written if possible
	 */
	public boolean setURI(String uriString) {
		URI uri;
		boolean status = true;
		
		if (! uriString.startsWith("http://")  && 
				! uriString.startsWith("https://")) 
			uriString = "http://" + uriString;
		
		try {
			uri = new URI(uriString);
			setURI(uri);
		} catch (URISyntaxException e) {
			status = false;
			write(e.toString());
		};
		return status;
	}	

	/**
	 * setURI - Set Tethys server address
	 * @param uri
	 */
	@SuppressWarnings("deprecation")
	public void setURI(URI uri) {
		
		if (! CertificateValidation &&
				uri.getScheme().contentEquals("https")) {
			// https - don't validate certificates
			Protocol proto = null;
			try {
				proto = new Protocol("https",
						new EasySSLProtocolSocketFactory(), uri.getPort());
			} catch (IOException e) {
				write(e.toString());
			} catch (GeneralSecurityException e) { 
				write(e.toString());
			}
			
			if (proto != null) {
				Protocol.registerProtocol("SelfSignedHTTPS", proto);
			}
			
		}
		resourceRoot = client.resource(
				UriBuilder.fromUri(uri).build());
	}
	
	/**
	 * setCollection
	 * @param collection - Collection to which we will import
	 */
	public void setCollection(String collection) {
		resourceCollection = resourceRoot.path(collection);
	}
	
	public void setImportMap(String map) {
		importMap = map;
		importspec.setSrcMap(map);
	}
	
	public void setSpeciesAbbreviationMap(String map) {
		speciesAbbreviationMap = map;
		importspec.setSppAbbrev(map);
	}
	
	private void dispose() {
		client.destroy();
	}
	/**
	 * add - Schedule a file to be included in import
	 * @param filename - Path to filename (string)
	 * @throws UniformInterfaceException 
	 */
	public void add(String filename)
		throws UniformInterfaceException {
		File file = new File(filename);
		add(file, null);
	}

	public void add(File file) {
		add(file, null);
	}	
		
	/**
	 * add - Add a file to the collection
	 * @param file - file to add
	 * @throws UniformInterfaceException
	 */
	public void add(File file, final UploadWorker worker) 
	 {
		
		//int pos = s.lastIndexOf('.');
		//if (pos > 0)
		//	return s.substring(0, pos);
		
		//File length() in bytes
		
		
		List<File> files = new ArrayList<File>();
		File zipped_file;
		String name = file.getName();
		int pos = name.lastIndexOf('.');
		String ext = name.substring(pos); 
		
		long filesize = file.length();
		
		try { //
			if(filesize > largefile   && ext.equals(".xml")){
				// Zip the xml to a temp file, XML compresses really well
				// todo:  We amy want to mark these files for deletion
				//        later, Windows 10 does not appear to have a
				//        policy that deletes temporary files.
				zipped_file = gzip_post(file);
				files.add(zipped_file);
			} else {
				
				files.add(file);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.add(files, worker);
	}
	
	/**
	 * gzip_post 
	 * Construct a zipped version of the file stored in a temporary location.
	 * Uses the original name followed by -zipped.gzip
	 * @param file
	 * @return gzipped File
	 * @throws IOException
	 */
	public File gzip_post(File file) throws IOException
	{
		String dir = file.getParentFile().toString();
		String basename = Util.stripExt(file);  // no path, no ext

		Path zipFile = Files.createTempFile(basename + "-temp-", "-zipped.gzip");
		File tmp = zipFile.toFile();
		GZIPOutputStream zipStream = new GZIPOutputStream(new FileOutputStream(tmp));

		// zip up file
		FileInputStream fis = new FileInputStream(file);	
		byte[] bytes = new byte[chunkSize];
		int length; 
		while ((length = fis.read(bytes)) >= 0) { 
			zipStream.write(bytes, 0, length); 
		} 

		fis.close();
		zipStream.close();
		
		return tmp;	
	}
	/**
	 * Add list of files to the specified collection
	 * @param files
	 * @param worker
	 */
	public void add(List<File> files, final UploadWorker worker) 
	 {
		
		guilog = worker;  // if non null, output by publishing to worker
		
		/*
		 * todo:  The worker progress bar (progBar) is not currently functioning
		 * with the ImportRESTful client.  It appears that there are ways
		 * to do this gracefully with Jersey API 2+, but we are currently
		 * usign Jersey 1.  Migrating and fixing is a low priority project.
		 */
		if(worker != null){
			client.removeAllFilters();
			
			// Only add progress bar when worker is specified and a single file
			// todo:  what kind of progress bar for multiple files?
			if (files.size() == 1) {				
				client.addFilter(new ConnectionListenerFilter(
						new ListenerFactory(worker.progBar)));
			}
		}
		
		// Build a multipart POST form to be submitted

		// Prepare the metadata file for attachment.
		// We do this before creating the multipart form as we
		// won't have to close it out if there's problems here
		// and we bail.
		
		for (File file:files) {
			// Verify valid filename
			if (file == null)
				continue;
			if(file.getName().contains(",") || file.getName().contains("&")){
				write("<Error>\n");
				write("  Comma and & not allowed in filenames\n");
				write("</Error>\n");
				return;
			}
			// Add the filename to the import specification
			importspec.add(file.getName(), "file");
		}
		
		// Initialize formdata, will hold the import specification
		// and any needed files.
		MediaType media = MultiPartMediaTypes.createFormData();
		FormDataMultiPart formdata = new FormDataMultiPart();
		formdata.setMediaType(media);
		
		// Set the import specification
		try {
			spec = importspec.toString();
			//setSpecification(formdata,spec);
			FormDataBodyPart part = 
					new FormDataBodyPart("specification", spec);
			formdata.bodyPart(part);
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
			return;
		}		

		for(File file:files){
			
			// Add file contents as form data
			FileDataBodyPart filepart;
			try {
				write(String.format("%s: %d bytes\n", 
						file.toString(), 
						file.length()));
				filepart = fileDataBodyPart(file.getName(), file);
			} catch (Throwable t) {
				filepart = null;
			}
			if (filepart == null) {
				return;  // error msg handled as a side effect
			}
			//formdata.field(file.getName(), bodypart, MediaType.APPLICATION_XML_TYPE);
			formdata.bodyPart(filepart); // attach file
		}

		try {
			formdata.close();  // no more bodyParts
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ClientResponse response;
		String responseText="";
		int status = 0;
		
		
		//try adding headers via WebResource:
		//https://jersey.java.net/apidocs/1.17/jersey/com/sun/jersey/api/client/WebResource.html#header(java.lang.String, java.lang.Object)
		WebResource web_import = resourceCollection.path("import");
		InputStream resIn;
		try {
			response = web_import.type(media)
					.header("Content-Encoding", "gzip")
					.accept(MediaType.APPLICATION_XML,MediaType.TEXT_PLAIN)
					.post(ClientResponse.class, formdata);
			resIn = response.getEntityInputStream();
			responseText = convertStreamToString(resIn);
			status = response.getStatus();

		} catch (UniformInterfaceException e) {
			write("<UniformInterfaceException>\n");
			write("  Response type was not expected\n");
			write("</UniformInterfaceException>\n");
			return;			
		} catch (ClientHandlerException e) {
			String msg = e.getMessage();
			boolean check_connection_msg = true;
			write("<Error>\n");
			if (msg != null) {
				write(String.format("  %s\n", msg));
				if (msg.contains("java.io.FileNotFoundException")) {
					check_connection_msg = false; // client-side error
				}
			}
			if (check_connection_msg == true) {
				write("  Unable to connect or connection refused, check Tethys server status with a web browser at:\n");
				write("    ");
				write(resourceRoot.path("Tethys").path("ping").toString());
				write("\n  If connection is refused by the web browser, the server is down,");
				write("\n  the network is not reachable (e.g., down or blocked by firewall rules)");
				write("\n  or there is an error in the server address.\n");
			}
			write("</Error>\n");
			formdata.cleanup();
			return;
		} catch (Exception e){
			/*
			 *  Save the error string
			 *  If the problem is that we required additional resources
			 *  to be attached (e.g., an audio file), we will resubmit
			 *  after analyzing the error mesage.
			 */
			write("<Error>\n  ");
			write(e.getMessage());
			write("</Error>\n");
			return;
		}

		if (status == BAD_REQUEST) {
			/*
			 * The document produced errors.  This may have been due to
			 * missing attachments.  We determine what attachments are
			 * needed by parsing the error message and building a new 
			 * submission.  
			 */

			if (guilog != null) {
				// Update state if attached to a GUI
				guilog.progBar.putClientProperty("state", State.ATTACHING);
			}
			checkAndResubmit(web_import, files.get(0), formdata, responseText,
					media,guilog);

		} else {
			write(responseText);
		}
		formdata.cleanup();
		
	}
	
	
	
	
	
	/**
	 * method to read response stream (necessary for chunked encoding only)
	 * @param is - the input stream to be convtd
	 * @return
	 */
	static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}
	
	
	// todo:  ODBC needs to be rewritten to handle new interfaces
	// The File interface already uses ODBC, only the connection
	// string stuff needs to be updated
	
	// odbc import for a file that will be transmitted across the network
	public void ODBC(File datafile, 
			String connectionStr, boolean overwrite, UploadWorker worker) 
	{
		
		guilog = worker;
		
		// Build a multipart POST form to be submitted
		FormDataMultiPart formdata = null;
		FileDataBodyPart bodypart = null;
				
		// Prepare the metadata file for attachment.
		// We do this before creating the multipart form as we
		// won't have to close it out if there's problems here
		// and we bail.
		//
		// bodyparts, oh my, sounds like a Quentin Tarantino movie
		try {
			bodypart = fileDataBodyPart("data", datafile);
		} catch (Throwable t) {
			bodypart = null;
			return;  // error msg handled as a side effect
		}
		
		formdata = new FormDataMultiPart();
		formdata.bodyPart(bodypart);  // attach the file
		
		setMaps(formdata);  // Set data abbreviation maps if appropriate
		if (overwrite == true) {
			FormDataBodyPart bp = new FormDataBodyPart("overwrite","1");
			formdata.bodyPart(bp);		
		}

		ClientResponse response;
		String responseText;
		WebResource odbc = resourceCollection.path("ODBC");
		int status = 0;
		
		try {
			response = odbc.type(MediaType.MULTIPART_FORM_DATA)
					.accept(MediaType.APPLICATION_XML,MediaType.TEXT_PLAIN)  //TEXT_PLAIN
					.post(ClientResponse.class, formdata);
			responseText = response.getEntity(String.class);
			status = response.getStatus();

		} catch (UniformInterfaceException e) {
			write("<UniformInterfaceException>\n");
			write("  Response type was not expected\n");
			write("</UniformInterfaceException>\n");
			return;			
		} catch (ClientHandlerException e) {
			write("<Error>\n");
			write("  Unable to connect or connection refused\n");
			write("</Error>\n");
			return;
		}
		
		if (status == 400 && datafile != null) {
			// User error, check if there's anything we can do about it.
			// In some cases we may be able to correct and resubmit.
			//TODO
			checkAndResubmit(odbc, datafile, formdata, responseText,null,null);
		} else {
			write(responseText);
		}
	}
	
	// odbc network resource
	public void ODBC(
			String connectionStr, UploadWorker worker) 
	{
		
		guilog = worker;
		

		// Build a multipart POST form to be submitted
		FormDataMultiPart formdata = new FormDataMultiPart();
		MediaType media = MultiPartMediaTypes.createFormData();
		FormDataBodyPart connPart = new FormDataBodyPart("ConnectionString", connectionStr);
		formdata.field("ConnectionString", connectionStr);
		if (importMap==null){
			write("<Error>\n");
			write("  Please select a SourceMap for ODBC imports\n");
			write("</Error>\n");
			return;
		}
		formdata.field("import_map", importMap);
		//setMaps(formdata);  // Set data abbreviation maps if appropriate
		//formdata.bodyPart(connPart);
		formdata.setMediaType(media);
		java.util.List<com.sun.jersey.multipart.BodyPart> parts = formdata.getBodyParts();

		ClientResponse response;
		String responseText;
		WebResource odbc = resourceCollection.path("ODBC");
		try {
			response = odbc.type(media)
					.accept(MediaType.APPLICATION_XML,MediaType.TEXT_PLAIN)  //TEXT_PLAIN
					.post(ClientResponse.class, formdata);
			responseText = response.getEntity(String.class);
		} catch (UniformInterfaceException e) {
			write("<UniformInterfaceException>\n");
			write("  Response type was not expected\n");
			write("</UniformInterfaceException>\n");
			return;			
		} catch (ClientHandlerException e) {
			write("<Error>\n");
			write("  Unable to connect or connection refused\n");
			write("</Error>\n");
			return;
		} catch (Exception e){
			write("<Error>\n");
			write(e.getMessage());
			write("</Error>\n");
			return;
		}
		
		write(responseText);
	}

	/**
	 * fileDataBodyPart - Add file as an attachment.  The form's field
	 * is specified via formName.
	 * @param formName
	 * @param file
	 * @return
	 */
	private FileDataBodyPart fileDataBodyPart(String formName, File file) {
		FileDataBodyPart part = null;
		
		if (checkFile(file)) {
			MediaType mediaType = predictor.getMediaTypeFromFile(file);
			part = new FileDataBodyPart(formName, file, mediaType);
		} 
		
		return part;
	}
	
	/**
	 * Verifies that a file exists and can be read.
	 * Writes an error message to the log file if it cannot.
	 * Return value of true indicates that everything is ok.
	 * @param datafile
	 * @return
	 */
	private boolean checkFile(File datafile) {

		boolean ok = true;  // until we learn otherwise
		if (! datafile.exists()) {
			fileError(datafile, "does not exist");
			ok = false;
		} else if (! datafile.canRead()) {
			fileError(datafile, "cannot be read");
			ok = false;
		}
		return ok;		
	}
	/**
	 * fileError - Note a problem with a file
	 * @param name
	 * @param cause
	 */
	private void fileError(File name, String cause) {
		write("<Error>\n");
		write("  <File>\n");
		write("    <Name>" + name + "</Name>\n");
		write("    <Cause> " + cause + " </Cause>\n");
		write("  </File>\n");
		write("</Error>\n");
	}
	
	
	/**
	 * Method to zip up attachment folder.
	 * @param dir - source doc dir
	 * @param basename - document basename
	 * @param imgFiles - linked list of expected img files
	 * @param audFiles - " audio
	 * @param missing - linked list to report missing files to
	 * @return linked list to be passed to addFiles containing zip's file path
	 * @throws IOException
	 */
	
	private LinkedList<String> zipAttachments(String dir, String basename, 
			LinkedList<String> imgFiles,LinkedList<String> audFiles, LinkedList<String> missing)
					throws IOException{
		
		boolean zipExists = false; //maybe already zipped?
		LinkedList<String> zip = new LinkedList<String>();
		final int bufferSize = 4096;
			
		
		// Try to find correct directory
				
		//New naming scheme, tested
		String attachDir = dir + "/" + basename + "-attach/" ;
		File attachFolder = new File(attachDir); 
			//Matlab uses 1.6 which does not have java.nio.Paths
			//Paths.get(dir, basename+ "-" + attachmentType).toFile();


		//old naming scheme
		if (! attachFolder.exists()) {
			attachDir = dir + "/" + basename + "-";
			// no java.nio.Paths: Paths.get(dir, basename, attachmentType).toFile();
		}


		String zipFile = dir + "\\" + basename + "-attach.zip";
		File tmp = new File(zipFile);
		if (tmp.exists() && tmp.isHidden()){
			tmp.delete();
		}else if (tmp.exists() && !(tmp.isHidden())){
			zipExists = true;
		}
		if (!zipExists){
			ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(tmp));
			tmp.deleteOnExit();
			//compression level
			zos.setLevel(Deflater.DEFAULT_COMPRESSION);
			//hide from windows--WINDOWS ONLY
			String os = System.getProperty("os.name");
			if (os.startsWith("Windows")){
				String exec = "attrib +H "+ "\""+ zipFile+"\"";
				Runtime.getRuntime().exec(exec);
			}
			if (imgFiles!=null && !zipExists){
				for (String imgFName: imgFiles){
					File file = new File(attachDir + "image/"+imgFName);
					if (!file.exists()){
						missing.add("Missing: "+file.getName());
						continue;
					}
					//only zip if everything is present
					if(missing.isEmpty()){
						zos.putNextEntry(new ZipEntry("image/"+file.getName()));
						BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));

						long bytesRead = 0;
						byte[] bytesIn = new byte[bufferSize];
						int read = 0;
						while((read = bis.read(bytesIn))!=-1){
							zos.write(bytesIn,0,read);
							bytesRead+=read;
						}
						zos.closeEntry();
						bis.close();
					}else{
						//TODO better message
						write("Missing attachment files");
					}
				}
			}
			if (audFiles!=null && !zipExists){
				for (String audFName: audFiles){
					File file = new File(attachDir + "audio/"+audFName);
					if (!file.exists()){
						missing.add(file.getName());
						continue;
					}
					if(missing.isEmpty()){
						zos.putNextEntry(new ZipEntry("audio/"+file.getName()));
						BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));

						@SuppressWarnings("unused")
						long bytesRead = 0;
						byte[] bytesIn = new byte[bufferSize];
						int read = 0;
						while((read = bis.read(bytesIn))!=-1){
							zos.write(bytesIn,0,read);
							bytesRead+=read;
						}
						zos.closeEntry();
						bis.close();

					}
				}
			}

			zos.flush();
			zos.close();
			if(!missing.isEmpty()){
				File trash = new File(zipFile);
				trash.delete();
			}
		}
		zip.add(zipFile);
		return zip;

	}

	private void write(String msg) {
		// Server message frequently formats for HTML
		String escaped_msg = StringEscapeUtils.unescapeHtml4(msg);
		if (guilog != null) {
			guilog.write(escaped_msg);
		} else {
			if (log != null)
				try {
					log.write(escaped_msg);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}
	
	/**
	 * Analyze the XML result message to see if there are errors that
	 * we can fix (missing media).  Resubmit if possible
	 * @param submitTo
	 *    (@link WebResource) REST server
	 * @param file
	 *    (@link File) source file to be transmitted
	 * @param formdata
	 *     (@link FormDataMultiPart) form variables to be transmitted
	 * @param xmlstr
	 *     (@link String) with results from submission
	 * @param mediatype
	 *      File's (@link MediaType)
	 * @param worker
	 *      (@link UploadWorker) thread that will perform upload
	 */
	private void 
	checkAndResubmit(WebResource submitTo, File file, 
			FormDataMultiPart formdata, String xmlstr, 
			MediaType mediatype, UploadWorker worker) {
		
		boolean zipped = false;
		long bytes = 0;  // todo:  Add in Excel file bytes
		
		StringWriter result = new StringWriter();
		
		// Build an environment for running XPath queries on the 
		// XML string that was passed in.
		
		InputStream xmlstream = new ByteArrayInputStream(xmlstr.getBytes());
		
		// Create something to parse the XML
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
				
		Document doc = null;
		try {
			doc = builder.parse(xmlstream);
		} catch (SAXException e1) {
			write("Unable to parse XML\n<!-- begin XML-->\n");
			write(xmlstr);
			write("\n<!-- end XML -->\n");
			return;
		} catch (IOException e1) {
			// shouldn't happen, we're reading a string after all.
			throw new RuntimeException("Problem reading XML\n" + xmlstr);
		}
		
		XPathFactory xpathFactory = XPathFactory.newInstance();
		XPath xpath = xpathFactory.newXPath();

		// See if there were any errors that we cannot handle
		NodeList errors;
		try {
			// We can handle missing media errors, find anything else
			errors = (NodeList) xpath.evaluate(
				"//Import/Document/*[not(self::Media/Missing) and not(self::Media/Warnings)]",
				doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			write("<Import>\n");
			write("  <Client>\n");
			write("    <Errors>\n");
			write("       <XPath> Unable to evaluate query to check " +
					"for non-recoverable errors </XPath>\n");
			write("    </Erors>\n");
			write("  </Client>\n");
			write("</Import>\n");
			write(result.toString());
			return;
		}

		if (errors.getLength() > 0) {
			write(xmlstr);  // Can't fix it, bail out
			return;
		}

		HashMap<String, LinkedList<String>> media = 
				MissingMedia(doc, xpath);


		String dir = file.getParentFile().toString();
		String basename = Util.stripExt(file);  // no path, no ext
		LinkedList<String> missingFiles = new LinkedList<String>();
		LinkedList<String> zipFile=new LinkedList<String>();


		//count number of files within the media hashmap
		int fileCount = 0;
		LinkedList<String> imgFiles = null;
		LinkedList<String> audFiles = null;
		for (Entry<String, LinkedList<String>> entry : media.entrySet()) {			
			String type = entry.getKey();
			LinkedList<String> files = new LinkedList<String>();
			files = entry.getValue();
			fileCount = fileCount+files.size();
			if (type == "Image" && files.size() > 0)
				imgFiles = files;
			else if (type== "Audio" && files.size() > 0)
				audFiles = files;
		}


		//Decide what to do with attachments depending on count
		//if no files, return 0
		if (fileCount == 0){
			bytes = bytes + 0;
		} else if (fileCount > 25){
			// Zip when there are lots of files
			try {
				zipFile = zipAttachments(dir, basename,imgFiles,audFiles,missingFiles);
				zipped = true;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bytes= bytes+ addFiles(formdata, zipFile, "Zip",
					dir, basename,missingFiles);
			
		} else {
			// Not enough to zip, add files individually
			if (imgFiles!=null) {
				bytes = bytes + 
				addFiles(formdata, imgFiles, "Image", 
						dir, basename, missingFiles);
			}
			if (audFiles!=null) {
				bytes = bytes + 
				addFiles(formdata, audFiles, "Audio", 
						dir, basename, missingFiles);
			}
		}


		if (missingFiles.size() == 0) {
			
			long totalBytes = worker.getBytes() + bytes;
			worker.setBytes(totalBytes);
			worker.progBar.putClientProperty("bytes", totalBytes);

			
			if (log != null) { 
				String msg = "<!-- Uploading " + Math.ceil((bytes/MB)*10)/10
				   + " MB of ancillary data... -->\n";
				write(msg);
			}
				
			// We were able to add everything, submit the new form
			try{
				ClientResponse response = submitTo
						.type(mediatype)
						.accept(MediaType.APPLICATION_XML)
						.post(ClientResponse.class, formdata);
				InputStream resIn = response.getEntityInputStream();
				String responseText = convertStreamToString(resIn);
				write(responseText);	
			}catch (ClientHandlerException cHE){
				//try again without chunked encoding
				//System.out.println("CHUNKED BROKE, ATTEMPTING STREAM");
				resubmitStreaming(formdata, mediatype);
				
			}catch (Exception e){
				e.printStackTrace();
			}
			
			
			
			
			
			if(zipped == true){
				File zip = new File(zipFile.element());
				if (zip.isHidden())
					zip.delete();
			}
		} else {
			write("<Import>\n");
			write("  <Client>\n");
			write("    <Errors>\n");
			//changing to Missing, change "problems" to missing files
			write("       <Missing>\n");
			for (String item : missingFiles) {
				write("         <Item> ");
				write(item);
				write(" </Item>\n");
			}
			write("       </Missing>\n");
			write("    </Errors>\n");
			write("  </Client>\n");
			write("</Import>\n");
			
		}
		
		write(result.toString());
	}
	
	private void resubmitStreaming(FormDataMultiPart form, MediaType mt){
		//create a new streaming client and webresource
		//by now it should have already been checked, and 
		//attachments already added to the form
		config = new DefaultApacheHttpClientConfig();
		client = ApacheHttpClient.create(config);
		client.removeAllFilters(); //no listener needed for chunked
		this.setURI(guilog.getURI());
		this.setCollection(guilog.getResource());
		WebResource resubmit = resourceCollection.path("import");
		guilog.progBar.putClientProperty("state", State.STREAMING);
		//TODO: color change? need JTextPane, not Area. Areas plaintext only, formatting would span all text.
		String streamStr = "<!--Normal import method failed, attempting alternative -->"+
							"<!--You will be notified when data transfer is complete...-->";
		write(streamStr);
		
		//try to submit it again
		try{
			ClientResponse response = resubmit.type(mt).accept(MediaType.APPLICATION_XML).post(ClientResponse.class,form);
			InputStream resIn = response.getEntityInputStream();
			String responseText = convertStreamToString(resIn);
			write(responseText);
		}catch (Exception e){
			e.printStackTrace();
		}
		
	}
	
	/**
	 * addFiles - List of files that are attached to an XML document
	 * e.g. image or audio data
	 * Given a list of files and a form, add them to the form
	 * so that they can be uploaded.
	 * @param formdata - form to be submitted
	 * @param files - List of files to upload
	 * @param attachmentType - What type of attachment (e.g. image/audio)
	 * @param dir - The base directory from which we search for the attachments
	 * @param basename - Attachment directory will be related to this name,
	 *                   either dir/basename/attachmentType or
	 *                   dir/basename-attachementType
	 * @param problems - An list of strings.  Any files that cannot be
	 * 					 found will be added to this.
	 * @return - # of bytes in the files that could be found.
	 */
	private long addFiles(FormDataMultiPart 
			formdata, LinkedList<String> files, 
			String attachmentType, String dir, String basename, 
			LinkedList<String> problems) {
		
		long bytes = 0;		// # bytes to upload


		// Try to find correct directory
		//check if attachments were zipped
		if (attachmentType != "Zip") {
			
			/* For given attachment type and document basename,
			 * e.g., audio, xyzzy
			 * see if there is an appropriate directory
			 * e.g. xyzzy-audio or xyzzy-attach/audio 
			 */
			Path[] paths = {
					filesys.getPath(dir, basename + "-" + attachmentType),
					filesys.getPath(dir, basename + "-attach", attachmentType)
			};
			Path attachPath = null;
			File attachDir = null;
			boolean found = false;
			for (Path path : paths) {
				attachPath = path;
				attachDir = path.toFile();
				if (attachDir.exists()) {
					found = true;
					break;
				}
			}

			if (! found) {
				// No suitable directory
				StringBuilder msg = new StringBuilder("Unable to find ");
				msg.append(attachmentType);
				msg.append(" attachments in one of: ");
				for (Path path : paths) {
					msg.append(path.toString());
					msg.append(" ");
				}
				problems.add(msg.toString());
				return bytes;
			}

			for (String f : files) {
				// Try to access file
				Path file_path = attachPath.resolve(f);
				File file = file_path.toFile();

				if (! file.exists())
					problems.add(f);
				else {
					MediaType mt = predictor.getMediaTypeFromFileName(f);
					FileDataBodyPart bodypart = new FileDataBodyPart(
							"Attachment", file, mt);
					formdata.bodyPart(bodypart);
					bytes = bytes + file.length();
				}
			}	
		} else {
			// attachments are zipped already
			String f = files.get(0);
			File zipfile = new File(f);
			MediaType mt = predictor.getMediaTypeFromFileName(f);
			FileDataBodyPart bodypart = new FileDataBodyPart(
					"Attachment", zipfile, mt);
			formdata.bodyPart(bodypart);
			bytes = bytes + zipfile.length();
		}

		return bytes;
	}
	
	private HashMap<String, LinkedList<String>>
	MissingMedia(Document doc, XPath xpath) {
		
		HashMap<String, LinkedList<String>> missing =
				new HashMap<String, LinkedList<String>>();
		
		// find media items for each type that were not sent with the import
		
		try {
			missing.put("Audio", getXPathList(doc, xpath,
			  "//Import/Document/Media/Missing/Issue[Description/text()=\"Audio\"]/Detail/text()"));
		} catch (XPathExpressionException e) {
			throw new RuntimeException("Error evaluating XPath for missing audio");
		}

		try {
			missing.put("Image", getXPathList(doc, xpath,
			  "//Import/Document/Media/Missing/Issue[Description/text()=\"Image\"]/Detail/text()"));
		} catch (XPathExpressionException e) {
			throw new RuntimeException("Error evaluating XPath for missing image");
		}

		return missing;

	}
	
	private LinkedList<String> getXPathList(Document doc, XPath xpathObj, String query) 
			throws XPathExpressionException {
		
		LinkedList<String> items = new LinkedList<String>();
		
		Object result = xpathObj.evaluate(query, doc, XPathConstants.NODESET);
		NodeList nodes = (NodeList) result;
		for (int i = 0; i < nodes.getLength(); i++)
			items.add(nodes.item(i).getNodeValue());
		
		return items;
	}
	
	/**
	 * Returns the JAXB Jersey Client
	 * @return
	 */
	public Client getClient(){
		return this.client;
	}
	
	/**
	 * setOverwrite
	 * Note whether or not we should overwriting data
	 * @param overwrite
	 */
	public void setOverwrite(boolean overwrite) {
			this.importspec.setOverwrite(overwrite);
	}

	

	/**
	 * Add body part for specification string for POST import
	 * @param formdata
	 * @param specification
	 */
	private void setSpecification(FormDataMultiPart formdata, String specification) {

		FormDataBodyPart bp = new FormDataBodyPart(
				"specification", specification);
		//MediaType.APPLICATION_XML_TYPE);

		formdata.bodyPart(bp);
	}

	
	/**
	 * setMaps
	 * Use class instance variables to populate the import and species
	 * abbreviation maps if they have been set.
	 * These have been switched from .field to BodyParts
	 * @param formdata
	 */

	private void setMaps(FormDataMultiPart formdata) {
		if (importMap != null){
			FormDataBodyPart bp = new FormDataBodyPart("import_map",importMap);
			formdata.bodyPart(bp);
			//formdata = formdata.field("import_map", importMap);
		}
		if (speciesAbbreviationMap != null){
			FormDataBodyPart bp = new FormDataBodyPart("species_abbr", speciesAbbreviationMap);
			formdata.bodyPart(bp);
			//formdata = formdata.field("species_abbr", speciesAbbreviationMap);
		}
	}
};
