
package dbxml.uploader;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.xml.stream.*;
import javax.xml.stream.events.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import dbxml.uploader.SourcePanel.SourceTypes;

/**
 * This class writes Specification XML as determined by
 * the options passed to UploadWorker from the ImportFrame GUI
 * 
 * @author sherbert
 *
 */


//TODO prettyprint


public class SpecWriter{


	/**
	 * fields
	 */

	private final LinkedList<SourcePanel> sources = new LinkedList<SourcePanel>();
	//class stuff
	XMLOutputFactory outputFactory;
	XMLEventWriter eventWriter;
	XMLEventFactory eventFactory;
	StartDocument startDocument;
	ByteArrayOutputStream stream;
	UploadWorker worker;
	
	
	//GUI Info
	String speciesAbbrev;
	String sourceMap;
	String docName;
	boolean overwrite;
	
	
	/**
	 * Constructors
	 */

	/**
	 * New style GUI Specification Writer.
	 */
	public SpecWriter(){
		// create an XMLOutputFactory
		this.outputFactory = XMLOutputFactory.newInstance();
		try{
			// create an EventFactory 
			this.eventFactory = XMLEventFactory.newInstance();
			// create and write XML Prolog
			this.startDocument = eventFactory.createStartDocument();

		}catch (Exception e){
			e.printStackTrace();
		}
		
	}
	
	
	
	
	
	/**
	 * Sets - store info from the gui
	 */
	
	
	public void setOverwrite(boolean b){
		this.overwrite = b;
	}
	
	public void setSppAbbrev(String guiAbbrev){
		this.speciesAbbrev = guiAbbrev;
	}
	
	public void setSrcMap(String guiMap){
		this.sourceMap = guiMap;
	}
	
	public void setDocName(String docname){
		this.docName = docname;
	}
	
	/* Getters */
	
	/**
	 * 
	 * @return overwrites permitted True|False
	 */
	public Boolean getOverwrite(){
		return this.overwrite;
	}
	
	/**
	 * @return species abbreviations map name
	 */
	public String getSppAbbrev(){
		return this.speciesAbbrev;
	}
	
	/**
	 * 
	 * @return source map used to translate from user data to Tethys
	 */
	public String getSrcMap(){
		return this.sourceMap;
	}
	
	/**
	 * 
	 * @return document name
	 */
	public String getDocName(){
		return this.docName;
	}
	

	
	/*
	 * GUI Source Panel Methods
	 */
	
	void add(SourcePanel source){
		this.sources.add(source);
	}
	
	SourcePanel getFirstSource(){
		return this.sources.getFirst();
	}
	
	String getFirstSourceName(){
		return this.sources.getFirst().getSourceName();
		
	}
	
	/**
	 * This method will guess a document name based on the sources it has.
	 * @return
	 */
	String guessDocName(){
		String def = "Document Name";
		String name = "";
		if(sources.isEmpty()){
			return def;
		}else{
			//loop through the sources...
			int i = 0;
			while (name.isEmpty()){
				SourcePanel source = this.sources.get(i);
					//first, see if its a file
					if (source.getType() == SourceTypes.FILE){
						//use the file's basename
						name = source.getBasename();
						if(name.isEmpty())
							//use the source name
							name = source.getSourceName();
					}else{
						//it is a resource, so try the source name
						if(source.getSourceName().isEmpty())
							continue; //move on to the next source
						else
							name = source.getSourceName();
					}
					i++;
					//we have reached the end of sources, lets name it default
					if (this.sources.size() == i && name.isEmpty())
						name = def;
				
			}
			return name;
		}
	}

	List<File> getSourceFiles(){
		List<File> files = new ArrayList<File>();
		if (!sources.isEmpty()){
			for (SourcePanel source:this.sources){
				if (source.getType() == SourceTypes.FILE){
					//getFile is inverse of setFile, and setFile is
					// only called when drag/dropped, or browse button used
					File sourceFile = source.getFile();
					if (sourceFile != null){
						files.add(sourceFile);
					}else{
						//if path was typed manually, or ctrl+v'd, need to add it manually.
						String fpath = source.getFilePath();
						if(fpath!=null){
							source.setDropped(new File(fpath));
							sourceFile = source.getFile();
							files.add(sourceFile);
						}else
							continue;
					}
					
				}
					
			}
		}else
			return Collections.emptyList();
		return files;
	}
	
	/**
	 * Returns true if there's MORE THAN ONE source..
	 * @return
	 */
	boolean hasSources() {
		return (this.sources.size() > 1);
	}
	
	/**
	 * Returns true if there are NO sources
	 * @return
	 */
	boolean isEmpty(){
		return this.sources.isEmpty();		
	}
	public void clearSources() {
		this.sources.clear();
		
	}

	
	
	/*
	 * WRITING METHODS
	 */
	
	/**
	 * Initialize specification document
	 * was fist located in constructor, moved
	 */
	public void initSpec(){
		this.stream = new ByteArrayOutputStream();
		// create XMLEventWriter
		try {
			eventWriter= this.outputFactory.createXMLEventWriter(stream);
			eventWriter.add(startDocument);

		} catch (XMLStreamException e) {
			e.printStackTrace();
		}

	}
	
	
	public String writeSpec() throws Exception {
		initSpec();
		// create import open tag
		StartElement specStartElement = eventFactory.createStartElement("",
				"", "import");
		eventWriter.add(specStartElement);

		// Write the different nodes
		createNode("docname", this.getDocName());
		createNode( "overwrite", this.getOverwrite().toString());
		createNode( "species_map", this.getSppAbbrev());
		createNode( "source_map", this.getSrcMap());

		
		//create the Sources node section
		createSources();
		
		//add sources
		for (int i=0;i<sources.size();i++){
			SourcePanel src = sources.get(i);
			addSource(src.getFileName(), src.getConnectionString(),src.getSourceName(), src.getType());
		}
		
		
		//close off Sources
		eventWriter.add(eventFactory.createEndElement("", "", "sources"));

		
		//close off document
		eventWriter.add(eventFactory.createEndElement("", "", "import"));

		eventWriter.add(eventFactory.createEndDocument());
		eventWriter.close();
		//return it as a string..?
		String xml = formatXml();
		return xml;
	}
	
	
	/**
	 * This only writes a single source spec. Works with old gui style
	 * @return
	 * @throws Exception
	 */

	String writeSpec(UploadWorker uw) throws Exception {
		//initialize XML
		initSpec();
		this.worker = uw;
		// create import open tag
		StartElement specStartElement = eventFactory.createStartElement("",
				"", "import");
		eventWriter.add(specStartElement);

		// Write the different nodes
		createNode("docname", this.getDocName());
		createNode( "overwrite", this.getOverwrite().toString());
		createNode( "species_map", this.getSppAbbrev());
		createNode( "source_map", this.getSrcMap());

		
		//create the Sources node section
		createSources();
		
		//add single sources
		addSource(this.worker.getImportFileName(), worker.getConnectionString(),null, SourceTypes.FILE);

		//close off Sources
		eventWriter.add(eventFactory.createEndElement("", "", "sources"));

		
		//close off document
		eventWriter.add(eventFactory.createEndElement("", "", "import"));

		eventWriter.add(eventFactory.createEndDocument());
		eventWriter.close();
		
		//pretty print
		String xml = formatXml();
		return xml;
	}


	private void createNode(String name,
			String value) throws XMLStreamException {
		// create Start node
		StartElement sElement = eventFactory.createStartElement("", "", name);
		eventWriter.add(sElement);
		// create Content
		Characters characters = eventFactory.createCharacters("");
		if(value!=null){
			characters = eventFactory.createCharacters(value);
		}
		eventWriter.add(characters);
		// create End node
		EndElement eElement = eventFactory.createEndElement("", "", name);
		eventWriter.add(eElement);

	}
	
	
	private String formatXml(){
		String xml = "";
		try{
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			StreamResult result = new StreamResult(new StringWriter());
			StreamSource source = new StreamSource(new StringReader(this.stream.toString()));
			transformer.transform(source, result);
			xml = result.getWriter().toString();
		}catch (Exception e){
			e.printStackTrace();
		}
		return xml;
	}
	
	/**
	 * Creates the Sources node
	 * @throws XMLStreamException
	 */
	
	public void createSources() throws XMLStreamException{
		eventWriter.add(eventFactory.createStartElement("", "","sources"));
	}
	
	

	
	
	
	
	/**
	 * Adds a Source under the Sources element, with connectionString
	 * 
	 * @throws XMLStreamException
	 */
	public void addSource(String fname, String connectionString, String name, SourceTypes type) throws XMLStreamException{
		//create <Source> node
		eventWriter.add(eventFactory.createStartElement("","","source"));


		//create the nodes within source

		createNode("type",type.toString().toLowerCase());
		if(fname != null)
			createNode("file", fname);
		if(name != null && !name.isEmpty())
			createNode("name",name);
		if(connectionString != null)
			createNode("connectionstring",connectionString);


		eventWriter.add(eventFactory.createEndElement("", "","source"));

	}
	
	public void remove(SourcePanel removeMe) {
		this.sources.remove(removeMe);		
	}






} 