package dbxml.uploader;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.ArrayList;




import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import com.sun.jersey.api.client.UniformInterfaceException;

import dbxml.uploader.ProgressMonitor.State;

public class UploadWorker extends SwingWorker<Void, String> {

	private ImportRESTful importer;
	private File nextImport;
	private String connectionString= null;
	private String map;	 // source map
	private String abbrev;  // species abbreviation
	private Writer log;
	
	private URI uri;
	private String resource;
	private SpecWriter writer;
	private long bytes;
	
	boolean overwrite;
	JProgressBar progBar;
	
	enum ImportMethods {
		FILE,
		DIR,
		ODBCFILE,
		ODBCNETWORKED,
		SOURCES
	};
	
	ImportMethods importMethod;

	
	/**
	 * UploadWorker - Swing worker to handle imports
	 * @param location - Server URL in string form
	 * @param resource - Name of resource, e.g. XQuery, Detections, etc.
	 * @param log - writer to use for logging
	 * @param prog - progress bar
	 */
	UploadWorker(String location, String resource, Writer log, JProgressBar prog){
		URI uri;
		if (! (location.startsWith("http://") || 
				location.startsWith("https://")))
			location = "http://" + location;
		try {
			uri = new URI(location);
		} catch (URISyntaxException e) {
			JOptionPane.showMessageDialog(null, "Server is not a valid URL (web address)");
			return;
		}
		commonInit(uri, resource, log, prog);
	}
	
	private void commonInit(URI uri, String resource, Writer log, JProgressBar bar) {
		this.log = log;
		this.progBar=bar;
		this.uri = uri;
		this.resource=resource;
		importMethod = ImportMethods.FILE;
	}
	
	
	
	//Sets and Gets
	
	void setBytes(long b){
		this.bytes = b;
	}
	
	public long getBytes(){
		return this.bytes;
	}
	
	public int getKBytes() {
		return (int) Math.ceil(this.bytes/1024.0);
	}
	
	public int getFileKBytes(){
		long fbytes = this.nextImport.length();
		return (int) Math.ceil(fbytes/1024.0);
	}
	
	
	public void setConnectionString(String connect) {
		this.connectionString = connect;
		//this.overwrite = false; //why is this here?
	}
	
	public boolean getOverwrite(){
		return this.writer.getOverwrite();
	}
	
	public String getConnectionString(){
		return this.connectionString;
	}

	public void setImport(String filename)
			throws UniformInterfaceException {
		nextImport = new File(filename);
	}

	public void setImport(File file) {
		nextImport = file;
	}
	public String getImportFileName(){
		return this.nextImport.getName();
	}
	
	public void setSpecWriter(SpecWriter writer){
		this.writer = writer;
	}
	
	public void setOverwrite(boolean overwrite){
		this.overwrite=overwrite;
		this.writer.setOverwrite(overwrite);
	}
	
	public void setImportType(ImportMethods type) {
		this.importMethod = type;
	}
	
	public ImportMethods getImportType(){
		return this.importMethod;
	}
	
	public void setImportMap(String map) {
		this.writer.setSrcMap(map);
		this.map = map;
	}
	
	public String getImportMap(){
		return this.writer.getSrcMap();
	}
	
	List<File> getSourceFiles(){
		return this.writer.getSourceFiles();
	}
	
	public void setSpeciesAbbreviationMap(String map) {
		abbrev = map;
		this.writer.setSppAbbrev(map); // no longer using? 
	}
	
	public String getSpeciesAbbreviationMap(){
		return this.writer.getSppAbbrev();
	}
	
	public ImportRESTful getImporter(){
		return this.importer;
	}
	
	public String writeSpecOld() throws Exception{
		return writer.writeSpec(this);
	}
	
	public String writeSpec() throws Exception{
		return writer.writeSpec();
	}
	
	public URI getURI(){
		return this.uri;
	}
	public String getResource(){
		return this.resource;
	}

	public static String getCurrentTime() {
		Calendar now = Calendar.getInstance();
		String currentTime = 
				String.format("%02d:", now.get(Calendar.HOUR_OF_DAY))
				+ String.format("%02d:", now.get(Calendar.MINUTE))
				+ String.format("%02d ", now.get(Calendar.SECOND))
				;
		return currentTime;
	}
	
	
	@Override
	protected Void doInBackground() throws Exception {
		// Process message in chunks when the amount of data to send
		// exceeds specified threshold chunk_thr
		int megabyte = 1024 * 1024;
		int chunk_thr = 10 * megabyte;
		boolean chunked = false;
		
		write("<!-- Import starting at " + getCurrentTime() + " -->\n");

		switch (importMethod) {
		case SOURCES:
			importer = new ImportRESTful(log, false);
			importer.setURI(uri);
			importer.setCollection(resource);
			List<File> fileList = getSourceFiles();
			boolean onlyNulls = isAllNulls(fileList);
			if(!onlyNulls)
				importer.add(fileList,this);
			else
				write("<Sources> Nothing to upload </Sources>\n");
			break;			
		case FILE:
			this.setBytes(nextImport.length());

			// Only one import is allowed.  If it is a directory,
			// we submit all eligible files independently in the directory.			
						
			boolean dir = nextImport.isDirectory();
			List<File> files = new ArrayList<File>();  // file(s) to submit 
			
			// Compute size of submission
			long totalBytes = 0;
			if (dir) {
				// User specified a directory.
				// Determine files that we could reasonably submit
				// and try to submit them one at a time.
				
				//filter for file extensions, code adapted from 
				//http://www.avajava.com/tutorials/lessons/how-do-i-use-a-filenamefilter-to-display-a-subset-of-files-in-a-directory.html
				FilenameFilter extFilter = new FilenameFilter() {
					public boolean accept(File dir, String name) {
						
						//TODO better way to do this?
						String[] okExt = {".xls",".xlsx",".xml",".csv"};
						String lowercaseName = name.toLowerCase();
						if (lowercaseName.endsWith(okExt[0])||
								lowercaseName.endsWith(okExt[1])||
								lowercaseName.endsWith(okExt[2])||
								lowercaseName.endsWith(okExt[3])) {
							return true;
						} else {
							return false;
						}
					}
				};
				
				// build list of files to submit
				for (File f : nextImport.listFiles(extFilter)) {
					totalBytes = totalBytes + f.length();
					files.add(f);
				}
			} else {
				// Only one file, get size & add to list
				totalBytes = nextImport.length();
				files.add(nextImport);
			}
			setBytes(totalBytes);
			// Prepare progress bar
			progBar.putClientProperty("bytes", totalBytes);
			progBar.putClientProperty("state", State.STARTED);
			
			// Iterate over files to submit
			
			for (File f : files) {
				// chunk large files
				chunked = f.length() > chunk_thr;
				// create importer
				importer = new ImportRESTful(log, chunked);
				importer.setURI(uri);
				importer.setCollection(resource);	
				importer.setOverwrite(overwrite);
				importer.setSpeciesAbbreviationMap(abbrev);
				importer.setImportMap(map);
				setImport(f);
				importer.add(f, this);
			}
			break;
			
		case ODBCFILE:
			importer = new ImportRESTful(log,chunked);
			if(this.map != null)
				importer.setImportMap(map);
			importer.setURI(uri);
			importer.setCollection(resource);
			importer.ODBC(nextImport, null, overwrite, this);
			break;
		case ODBCNETWORKED:
			importer = new ImportRESTful(log,chunked);
			if(this.map != null)
				importer.setImportMap(map);
			importer.setURI(uri);
			importer.setCollection(resource);
			importer.ODBC(connectionString, this);
		}
		return null;
	}

	public void write(String logentry) {
		publish(logentry);
	}

	@Override
	protected void process(List<String> entries) {

		try { 
			for (String entry : entries) {
				log.write(entry);
			}
			log.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
	@Override
	protected void done(){
		/*
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
			progress.setMaximum(0);
			progress.setIndeterminate(false);
			progress.setString("Finished Working");
			}
		});
		*/
		progBar.putClientProperty("state", State.FINISHED);
		write("<!-- Import attempt completed at " + UploadWorker.getCurrentTime() + "-->\n");
	}

	public boolean isAllNulls(Iterable<?> array) {
	    for (Object element : array)
	        if (element != null) return false;
	    return true;
	}




}
