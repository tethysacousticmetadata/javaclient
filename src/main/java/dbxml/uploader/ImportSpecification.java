package dbxml.uploader;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.LinkedList;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * ImportSpecification
 * Stand-alone class to represent the fields associated with
 * RESTful POST import transactions to a collection which
 * add data to Tethys
 * @author mroch
 *
 */
public class ImportSpecification {
	/**
	 * NameFile - Pair of source name, file name 
	 */
	public class Source {
		String source;  // source data (e.g. filename)
		String type;  // source type:  file | resource
		String name;  // source name
		String connection;  // connection string
		
		/**
		 * Source - specify a new source that will be used in importing
		 * a document.  Constructors can take 2 to 4 arguments:
		 * @param s - data source (e.g. a filename)
		 * @param t - data type:  file | resource (e.g. database) 
		 * @param n - data name:  In multiple source imports, data name
		 * 	is used in the source mapping between data fields and Tethys
		 *  fields.
		 * @param c - office database connectivity (ODBC) connection string
		 *  Mandatory for resource data types, otherwise the server
		 *  will try to guess from the file type.
		 * 
		 */

		public Source(String s, String t, String n, String c) {
			source = s;
			type = t;
			name = n;
			connection = c;
		}

		public Source(String s, String t, String n) {
			source = s;
			type = t;
			name = n;
			connection = null;
		}
		
		/**
		 * Source - specify a new source that will be used in importing
		 * a document.
		 * @param s - data source (e.g. a filename)
		 * @param t - data type:  file | resource (e.g. database) 
		 * 
		 * name will be unspecified, used for single source documents
		 */
		public Source(String s, String t) {
			source = s;
			type = t;
			name = null;
			connection = null;
		}
			
		
		public String getSource() {
			return this.source;
		}
		
		public String getType() {
			return this.type;
		}
		
		public String getName() {
			return this.name;
		}
		
		public String getConnection() {
			return this.connection;
		}
	}
	

	/**
	 * fields
	 */

	private final LinkedList<Source> sources = new LinkedList<Source>();
	//class stuff
	XMLOutputFactory outputFactory;
	XMLEventFactory eventFactory;	// Used to create XML documents
	
	
	// import properties
	String speciesAbbrev;
	String sourceMap;
	String docName;
	boolean overwrite;
	
	
	/**
	 * Constructors
	 */
	ImportSpecification() {
		// create an XMLOutputFactory
		this.outputFactory = XMLOutputFactory.newInstance();
		try {
			// create an EventFactory 
			this.eventFactory = XMLEventFactory.newInstance();
		} catch (Exception e){
			e.printStackTrace();
		}	
		overwrite = false;  // Never overwrite unless directed to do so
	}
	
	/**
	 * setters
	 */
	
	/**
	 * Control whether or not documents can be overwritten
	 * @param overwrite - True:  allow overwrite, False: forbid overwrite
	 */
	public void setOverwrite(boolean overwrite){
		this.overwrite = overwrite;
	}
	
	public void setSppAbbrev(String abbrev){
		this.speciesAbbrev = abbrev;
	}
	
	public void setSrcMap(String src2destmap){
		this.sourceMap = src2destmap;
	}
	
	public void setDocName(String docname){
		this.docName = docname;
	}
	
	/* Getters - retrieved stored information */
	
	/**
	 * Overwrites permitted?
	 */
	public Boolean getOverwrite(){
		return this.overwrite;
	}
	
	/*
	 * Current species abbreviation
	 */
	public String getSppAbbrev(){
		return this.speciesAbbrev;
	}
	
	/*
	 * Source map that will be used to translate user data to Tethys
	 */
	public String getSrcMap(){
		return this.sourceMap;
	}
	
	/*
	 * document name
	 */
	public String getDocName(){
		return this.docName;
	}
	

	/**
	 * add - Add a file to the set of data that will be imported
	 * @param resource- Name of resource (possibly filename)
	 * @param type - Resource type
	 */
	void add(String resource, String type){
		this.sources.add(new Source(resource, type));
	}
	
	/**
	 * add - Add a file to the set of data that will be imported
	 * @param resource- Name of resource (possibly filename)
	 * @param type - Resource type
	 * @param name - Name by which resource is referred to in the source map
	 */
	void add(String resource, String type, String name){
		this.sources.add(new Source(resource, type, name));
	}
	
	/**
	 * add - Add a file to the set of data that will be imported
	 * @param resource- Name of resource (possibly filename)
	 * @param type - Resource type
	 * @param name - Name by which resource is referred to in the source map
	 * @param connection - ODBC connection string
	 */
	void add(String resource, String type, String name, String connection) {
		this.sources.add(new Source(resource, type, name, connection));		
	}
		
	/**
	 * Generate the XML specification as a string.  Will not throw an error,
	 * but will write a bad string on failure
	 */
	public String toString() {
		String str;
		try {
			str = this.getSpecification();
		} catch (Exception e) {
			str = "Unable to convert import specification to string. Backtrace\n";	
			str = str + e.getStackTrace().toString();
		}
		return str;
	}
	
	/**
	 * Generates the XML specification used in the specification
	 * form variable of a POST to a collection with the "import" directive.
	 * 
	 * @return XML string
	 * @throws Exception
	 */
	public String getSpecification() throws Exception {
		
		// Initialize the document

		// underlying buffer
		ByteArrayOutputStream stream = 
				new ByteArrayOutputStream();  
		// Used to write XML elements
		XMLEventWriter xml = 
				this.outputFactory.createXMLEventWriter(stream);
		// Write XML preamble (encoding system)
		xml.add(eventFactory.createStartDocument());

		// create import open tag
		start(xml, "import");
		
		// Name of document, overwrite permissions,
		// species and source maps if specified
		element(xml, "docname", this.getDocName());
		element(xml, "overwrite", this.getOverwrite().toString());
		element(xml, "species_map", this.getSppAbbrev());
		element(xml, "source_map", this.getSrcMap());

		// List of sources
		start(xml, "sources");
		for (Source s : sources) {
			// Write out each source
			start(xml, "source");
			element(xml, "type", s.getType());
			element(xml, "file", s.getSource());
			element(xml, "name", s.getName());
			element(xml, "connectionstring", s.getConnection());
			end(xml, "source");
		}
		end(xml, "sources");		
		end(xml, "import");
		
		// close off the document, convert underlying stream to string & format
		xml.add(eventFactory.createEndDocument());
		xml.close();
		String xmlstr = formatXml(new StringReader(stream.toString()));
		return xmlstr;
	}
	

	/**
	 * Write out an element with an associated string.  Does not produce
	 * an element if no value is provided.
	 * @param xml - XMLEventWriter stream to which element will be written
	 * @param name - name of element
	 * @param value - value of element
	 * @throws XMLStreamException 
	 */
	private void element(XMLEventWriter xml, String name, String value) throws XMLStreamException {		
		// Only write if element has a value
		if (value != null) {
			// Write element open, value, close
			start(xml, name);
			xml.add(eventFactory.createCharacters(value));
			end(xml, name);
		}
		
	}
	/**
	 * Write out the start of an element that might have nested content
	 * @param xml - XMLEventWriter stream to which element will be written
	 * @param name - name of element
	 * @throws XMLStreamException 
	 */
	private void start(XMLEventWriter xml, String name) throws XMLStreamException {
		xml.add(eventFactory.createStartElement("", "", name));
	}
	/**
	 * Close off an opened element (start()).  Be careful of nesting!
	 * @param xml - XMLEventWriter stream to which element will be written
	 * @param name - name of element
	 * @throws XMLStreamException 
	 */
	
	private void end(XMLEventWriter xml, String name) throws XMLStreamException {
		xml.add(eventFactory.createEndElement("", "", name));
	}

	/**
	 * Take the XML contained in the event writer and transform it to a
	 * string
	 * @param xmlstr - XML materials
	 * @return formatted XML string
	 */
	private String formatXml(StringReader xmlstr){
		String xml;
		try {
			// Set up transformation to a text string
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

			// perform the transformation
			StreamSource source = new StreamSource(xmlstr);
			StreamResult result = new StreamResult(new StringWriter());
			transformer.transform(source, result);

			// convert to string
			xml = result.getWriter().toString();  
		} catch (Exception e){
			e.printStackTrace();
			xml = "Unable to form XML";
		}
		
		return xml;
	}
		
}
