package dbxml.uploader;

/*-- 

 Copyright (C) 2001 Brett McLaughlin.
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions, and the following disclaimer.
 
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions, and the disclaimer that follows 
    these conditions in the documentation and/or other materials 
    provided with the distribution.

 3. The name "Java and XML" must not be used to endorse or promote products
    derived from this software without prior written permission.  For
    written permission, please contact brett@newInstance.com.
 
 In addition, we request (but do not require) that you include in the 
 end-user documentation provided with the redistribution and/or in the 
 software itself an acknowledgement equivalent to the following:
     "This product includes software developed for the
      'Java and XML' book, by Brett McLaughlin (O'Reilly & Associates)."

 THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED.  IN NO EVENT SHALL THE JDOM AUTHORS OR THE PROJECT
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

 */
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.httpclient.contrib.ssl.EasySSLProtocolSocketFactory;
import org.apache.commons.httpclient.protocol.Protocol;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;













import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;



import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.client.apache.ApacheHttpClient;
import com.sun.jersey.client.apache.config.DefaultApacheHttpClientConfig;





// This is an XML book - no need for explicit Swing imports
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;
import javax.swing.tree.*;
import javax.ws.rs.core.UriBuilder;

/**
 * SAXTreeViewer uses Swing to graphically
 *   display an XML document.
 */
public class SAXTreeViewer extends JFrame {

    /** Default parser to use */
    private String vendorParserClass = 
        "javax.xml.parsers.SAXParser";

    /** The base tree to render */
    private JTree tree;
    
    private String uri;

    /** Tree model to use */
    DefaultTreeModel defaultTreeModel;
    
    
    /** RESTuff*/
    private WebResource resource;
    private Client client;
    private ClientConfig config;
    
    

    /**
     * <p> This initializes the needed Swing settings. </p>
     */
    public SAXTreeViewer() {
        // Handle Swing setup
        super("SAX Tree Viewer");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setNativeLookAndFeel();
        setSize(600, 450);

    }
    
    protected SAXTreeViewer(String xmlURI){
		config = new DefaultApacheHttpClientConfig();
		client = ApacheHttpClient.create(config);
    	this.uri = xmlURI;
    }

    /**
     * <p> This will construct the tree using Swing. </p>
     *
     * @param xmlURI path to XML document.
     */
    public void init(String xmlURI) throws IOException, SAXException {
        DefaultMutableTreeNode base = 
            new DefaultMutableTreeNode("XML Document: " + 
                xmlURI);
        
        // Build the tree model
        defaultTreeModel = new DefaultTreeModel(base);
        tree = new JTree(defaultTreeModel);
        
        MouseListener ml = new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                int selRow = tree.getRowForLocation(e.getX(), e.getY());
                TreePath selPath = tree.getPathForLocation(e.getX(), e.getY());
                if(selRow != -1) {
                    if(e.getClickCount() == 1) {
                        System.out.println("clicked");
                        System.out.println("Path: " + selPath.toString());
                    }
                    else if(e.getClickCount() == 2) {
                    	//System.out.println("double clicked");
                    }
                }
            }
        };
        tree.addMouseListener(ml);
        tree.setEditable(false);
        tree.getSelectionModel().setSelectionMode
        	(TreeSelectionModel.SINGLE_TREE_SELECTION);

        // Construct the tree hierarchy
        buildTree(defaultTreeModel, base, xmlURI);

        // Display the results
        getContentPane().add(new JScrollPane(tree), 
            BorderLayout.CENTER);
    }
    
    
    /**
     * Create and initialize JTree
     * @return tree
     */
    public JTree makeTree() throws IOException, SAXException {
        DefaultMutableTreeNode base = 
            new DefaultMutableTreeNode("XML Document: " + 
                uri);
        
        // Build the tree model
        defaultTreeModel = new DefaultTreeModel(base);
        tree = new JTree(defaultTreeModel);
        
        /* for future usage
         * detects clicks, double clicks, and the 'path' that was clicked
         * 
        MouseListener ml = new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                int selRow = tree.getRowForLocation(e.getX(), e.getY());
                TreePath selPath = tree.getPathForLocation(e.getX(), e.getY());
                if(selRow != -1) {
                    if(e.getClickCount() == 1) {
                        System.out.println("clicked");
                        System.out.println("Path: " + selPath.toString());
                    }
                    else if(e.getClickCount() == 2) {
                    	//System.out.println("double clicked");
                    }
                }
            }
        };
        tree.addMouseListener(ml);
        
        */
        
        tree.setEditable(false);
        tree.getSelectionModel().setSelectionMode
        	(TreeSelectionModel.SINGLE_TREE_SELECTION);
        
        //retrieve XML doc
        ClientResponse response;
        InputStream resIn;
        try{
    		resource = client.resource(
    				UriBuilder.fromUri(uri).build());
        }catch (IllegalArgumentException e){
        	String[] err = {"Error: " + e.getCause().getMessage()}; 
			return new JTree(err);//nothing to parse
        }

		try{
			response = resource.get(ClientResponse.class);
		}catch (ClientHandlerException e) {
			String[] cannotConnect={"Unable to connect or connection refused"};
			return new JTree(cannotConnect);
		}
		int status = response.getStatus();
		if(status == 404){
			String[] notFound = {"404 Not Found: " + uri}; 
			return new JTree(notFound);//nothing to parse
		}

		resIn = response.getEntityInputStream();

        // Construct the tree hierarchy
        buildTree(defaultTreeModel, base, resIn);
        //Return the results
        return tree;
    }

    
    

    /**
     * <p>This handles building the Swing UI tree.</p>
     *
     * @param treeModel Swing component to build upon.
     * @param base tree node to build on.
     * @param resIn InputStream to build XML document from.
     * @throws IOException - when reading the XML URI fails.
     * @throws SAXException - when errors in parsing occur.
     */
    public void buildTree(DefaultTreeModel treeModel, 
                          DefaultMutableTreeNode base,InputStream resIn) 
        throws IOException, SAXException {

        // Create instances needed for parsing
        XMLReader reader = 
            XMLReaderFactory.createXMLReader();
        ContentHandler jTreeContentHandler = 
            new JTreeContentHandler(treeModel, base);
        ErrorHandler jTreeErrorHandler = new JTreeErrorHandler();

        // Register content handler
        reader.setContentHandler(jTreeContentHandler);

        // Register error handler
        reader.setErrorHandler(jTreeErrorHandler);
        
		//String responseText = ImportRESTful.convertStreamToString(resIn);

        // Parse
        InputSource inputSource = 
            new InputSource(resIn);
        reader.parse(inputSource);
    }
    
    /**
     * This handles building the Swing UI tree.
     *
     * @param treeModel Swing component to build upon.
     * @param base tree node to build on.
     * @param xmlURI URI to build XML document from.
     * @throws IOException - when reading the XML URI fails.
     * @throws SAXException - when errors in parsing occur.
     */
    public void buildTree(DefaultTreeModel treeModel, 
                          DefaultMutableTreeNode base, String xmlURI) 
        throws IOException, SAXException {

        // Create instances needed for parsing
        XMLReader reader = 
            XMLReaderFactory.createXMLReader();
        ContentHandler jTreeContentHandler = 
            new JTreeContentHandler(treeModel, base);
        ErrorHandler jTreeErrorHandler = new JTreeErrorHandler();

        // Register content handler
        reader.setContentHandler(jTreeContentHandler);

        // Register error handler
        reader.setErrorHandler(jTreeErrorHandler);

        // Parse
        InputSource inputSource = 
            new InputSource(xmlURI);
        reader.parse(inputSource);
    }

    /**
     * <p> Static entry point for running the viewer. </p>
     */
    public static void main(String[] args) {
        try {
            if (args.length != 1) {
                System.out.println(
                    "Usage: java SAXTreeViewer " +
                    "[XML Document URI]");
                System.exit(0);
            }
            SAXTreeViewer viewer = new SAXTreeViewer();
            viewer.init(args[0]);
            viewer.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
	//L&F
	public void setNativeLookAndFeel() {
		try {
			UIManager.setLookAndFeel(UIManager
					.getSystemLookAndFeelClassName());
		} catch (Exception e) {
		}
	}
	
	
	/** RESTful stuff */

}

/**
 * JTreeContentHandler implements the SAX
 *   ContentHandler interface and defines callback
 *   behavior for the SAX callbacks associated with an XML
 *   document's content, bulding up JTree nodes.
 */
class JTreeContentHandler implements ContentHandler {

    /** Hold onto the locator for location information */
    private Locator locator;

    /** Store URI to prefix mappings */
    private Map namespaceMappings;

    /** Tree Model to add nodes to */
    private DefaultTreeModel treeModel;

    /** Current node to add sub-nodes to */
    private DefaultMutableTreeNode current;
    
    /**Counter for Detection*/
    private int detNum=0;

    /**
     * <p> Set up for working with the JTree. </p>
     *
     * @param treeModel tree to add nodes to.
     * @param base node to start adding sub-nodes to.
     */
    public JTreeContentHandler(DefaultTreeModel treeModel, 
                               DefaultMutableTreeNode base) {
        this.treeModel = treeModel;
        this.current = base;
        this.namespaceMappings = new HashMap();
    }

    /**
     * <p>
     *  Provide reference to Locator which provides
     *    information about where in a document callbacks occur.
     * </p>
     *
     * @param locator Locator object tied to callback
     *        process
     */
    public void setDocumentLocator(Locator locator) {
        // Save this for later use
        this.locator = locator;
    }

    /**
     * <p>
     *  This indicates the start of a Document parse-this precedes
     *    all callbacks in all SAX Handlers with the sole exception
     *    of {@link #setDocumentLocator}.
     * </p>
     *
     * @throws SAXException when things go wrong
     */
    public void startDocument() throws SAXException {
        // No visual events occur here
    }

    /**
     * <p>
     *  This indicates the end of a Document parse-this occurs after
     *    all callbacks in all SAX Handlers..
     * </p>
     *
     * @throws SAXException when things go wrong
     */
    public void endDocument() throws SAXException {
        // No visual events occur here
    }

    /**
     * <p>
     *   This indicates that a processing instruction (other than
     *     the XML declaration) has been encountered.
     * </p>
     *
     * @param target target of processing instruction
     * @param data data associated with the processing instruction
     *        This typically looks like one or more attribute value
     *               pairs.
     * @throws SAXException when things go wrong
     */
    public void processingInstruction(String target, String data)
        throws SAXException {

        DefaultMutableTreeNode pi = 
            new DefaultMutableTreeNode("PI (target = '" + target +
                                       "', data = '" + data + "')");
        current.add(pi);
    }

    /**
     * <p>
     *   This indicates the beginning of an XML Namespace prefix
     *     mapping. Although this typically occurs within the root element
     *     of an XML document, it can occur at any point within the
     *     document. Note that a prefix mapping on an element triggers
     *     this callback <i>before</i> the callback for the actual element
     *     itself ({@link #startElement}) occurs.
     * </p>
     *
     * @param prefix String prefix used for the namespace
     *                being reported
     * @param uri String URI for the namespace
     *               being reported
     */
    public void startPrefixMapping(String prefix, String uri) {
        // No visual events occur here.
        namespaceMappings.put(uri, prefix);
    }

    /**
     * <p>
     *   This indicates the end of a prefix mapping, when the namespace
     *     reported in a {@link #startPrefixMapping} callback
     *     is no longer available.
     * </p>
     *
     * @param prefix String of namespace being reported
     */
    public void endPrefixMapping(String prefix) {
        // No visual events occur here.
        for (Iterator i = namespaceMappings.keySet().iterator(); 
             i.hasNext(); ) {

            String uri = (String)i.next();
            String thisPrefix = (String)namespaceMappings.get(uri);
            if (prefix.equals(thisPrefix)) {
                namespaceMappings.remove(uri);
                break;
            }
        }
    }

    /**
     * <p>
     *   This reports the occurrence of an actual element. It includes
     *     the element's attributes, with the exception of XML vocabulary
     *     specific attributes, such as
     *     xmlns:[namespace prefix] and
     *     xsi:schemaLocation.
     * </p>
     *
     * @param namespaceURI String namespace URI this element
     *               is associated with, or an empty String
     * @param localName String name of element (with no
     *               namespace prefix, if one is present)
     * @param qName String XML 1.0 version of element name:
     *                [namespace prefix]:[localName]
     * @param atts Attributes list for this element
     * @throws SAXException when things go wrong
     */
    public void startElement(String namespaceURI, String localName,
                             String qName, Attributes atts)
        throws SAXException {
    	
    	String nodeStr = "Element: " + localName;
    	//add a number for Detection
    	if (localName == "Detection"){
    		detNum = detNum + 1;
    		nodeStr = nodeStr +"-"+Integer.toString(detNum);
    	}
        DefaultMutableTreeNode element = 
            new DefaultMutableTreeNode(nodeStr);
        current.add(element);
        current = element;

        // Determine namespace
        if (namespaceURI.length() > 0) {
            String prefix = 
                (String)namespaceMappings.get(namespaceURI);
            if (prefix.equals("")) {
                prefix = "[None]";
            }
            DefaultMutableTreeNode namespace =
                new DefaultMutableTreeNode("Namespace: prefix = '" +
                    prefix + "', URI = '" + namespaceURI + "'");
            current.add(namespace);
        }

        // Process attributes
        for (int i=0; i<atts.getLength(); i++) {
            DefaultMutableTreeNode attribute =
                new DefaultMutableTreeNode("Attribute (name = '" +
                                           atts.getLocalName(i) + 
                                           "', value = '" +
                                           atts.getValue(i) + "')");
            String attURI = atts.getURI(i);
            if (attURI.length() > 0) {
                String attPrefix = 
                    (String)namespaceMappings.get(namespaceURI);
                if (attPrefix.equals("")) {
                    attPrefix = "[None]";
                }
                DefaultMutableTreeNode attNamespace =
                    new DefaultMutableTreeNode("Namespace: prefix = '" +
                        attPrefix + "', URI = '" + attURI + "'");
                attribute.add(attNamespace);            
            }
            current.add(attribute);
        }
    }

    /**
     * <p>
     *   Indicates the end of an element
     *     (&lt;/[element name]&gt;) is reached. Note that
     *     the parser does not distinguish between empty
     *     elements and non-empty elements, so this occurs uniformly.
     * </p>
     *
     * @param namespaceURI String URI of namespace this
     *                element is associated with
     * @param localName String name of element without prefix
     * @param qName String name of element in XML 1.0 form
     * @throws SAXException when things go wrong
     */
    public void endElement(String namespaceURI, String localName,
                           String qName)
        throws SAXException {

        // Walk back up the tree
        current = (DefaultMutableTreeNode)current.getParent();
    }

    /**
     * <p>
     *   This reports character data (within an element).
     * </p>
     *
     * @param ch char[] character array with character data
     * @param start int index in array where data starts.
     * @param length int index in array where data ends.
     * @throws SAXException when things go wrong
     */
    public void characters(char[] ch, int start, int length)
    		throws SAXException {

    	String s = new String(ch, start, length);
    	if(!(s.trim().length() == 0)){
    		DefaultMutableTreeNode data =
    				new DefaultMutableTreeNode("'" + s + "'");
    		current.add(data);
    	}

    }

    /**
     * <p>
     * This reports whitespace that can be ignored in the
     * originating document. This is typically invoked only when
     * validation is ocurring in the parsing process.
     * </p>
     *
     * @param ch char[] character array with character data
     * @param start int index in array where data starts.
     * @param length data length.
     * @throws SAXException when things go wrong
     */
    public void ignorableWhitespace(char[] ch, int start, int length)
        throws SAXException {
        
        // This is ignorable, so don't display it
    }

    /**
     * <p>
     *   This reports an entity that is skipped by the parser. This
     *     should only occur for non-validating parsers, and then is still
     *     implementation-dependent behavior.
     * </p>
     *
     * @param name String name of entity being skipped
     * @throws SAXException when things go wrong
     */
    public void skippedEntity(String name) throws SAXException {
        DefaultMutableTreeNode skipped =
            new DefaultMutableTreeNode("Skipped Entity: '" + name + "'");
        current.add(skipped);
    }
}

/**
 * <b>JTreeErrorHandler</b> implements the SAX
 *   ErrorHandler interface and defines callback
 *   behavior for the SAX callbacks associated with an XML
 *   document's warnings and errors.
 */
class JTreeErrorHandler implements ErrorHandler {

    /**
     * <p>
     * This will report a warning that has occurred; this indicates
     *   that while no XML rules were "broken", something appears
     *   to be incorrect or missing.
     * </p>
     *
     * @param exception SAXParseException that occurred.
     * @throws SAXException when things go wrong 
     */
    public void warning(SAXParseException exception)
        throws SAXException {
            
        System.out.println("**Parsing Warning**\n" +
                           "  Line:    " + 
                              exception.getLineNumber() + "\n" +
                           "  URI:     " + 
                              exception.getSystemId() + "\n" +
                           "  Message: " + 
                              exception.getMessage());        
        throw new SAXException("Warning encountered");
    }

    /**
     * This will report an error that has occurred; this indicates
     *   that a rule was broken, typically in validation, but that
     *   parsing can reasonably continue.
     *
     * @param exception SAXParseException that occurred.
     * @throws SAXException when things go wrong 
     */
    public void error(SAXParseException exception)
        throws SAXException {
        
        System.out.println("**Parsing Error**\n" +
                           "  Line:    " + 
                              exception.getLineNumber() + "\n" +
                           "  URI:     " + 
                              exception.getSystemId() + "\n" +
                           "  Message: " + 
                              exception.getMessage());
        throw new SAXException("Error encountered");
    }

    /**
     * <p>
     * This will report a fatal error that has occurred; this indicates
     *   that a rule has been broken that makes continued parsing either
     *   impossible or an almost certain waste of time.
     * </p>
     *
     * @param exception that occurred.
     * @throws SAXException when things go wrong 
     */
    public void fatalError(SAXParseException exception)
        throws SAXException {
    
        System.out.println("**Parsing Fatal Error**\n" +
                           "  Line:    " + 
                              exception.getLineNumber() + "\n" +
                           "  URI:     " + 
                              exception.getSystemId() + "\n" +
                           "  Message: " + 
                              exception.getMessage());        
        throw new SAXException("Fatal Error encountered");
    }
    
    
    

}
// Demo file: book.xml
/*
<?xml version="1.0"?>

<games>
  <game genre="rpg">XML Invaders</game>
  <game genre="rpg">A Node in the XPath</game>
  <game genre="rpg">XPath Racers</game>
</games>

*/