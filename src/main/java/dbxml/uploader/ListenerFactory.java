package dbxml.uploader;

import javax.swing.JProgressBar;

import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.filter.OnStartConnectionListener;
import com.sun.jersey.api.client.filter.ContainerListener;

public class ListenerFactory implements OnStartConnectionListener {
	
	JProgressBar progressBar;
	ListenerFactory(JProgressBar prog){
		setProgressBar(prog);
	}
	
	public void setProgressBar(JProgressBar prog){
		this.progressBar = prog;
	}


	public ContainerListener onStart(ClientRequest cr) {
		return new ProgressMonitor(progressBar);
	}
}
