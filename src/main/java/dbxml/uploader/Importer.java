/*
 * Class used for uploading files.
 * Contains static methods that can be called.
 */
package dbxml.uploader;

import java.io.File;
import java.io.StringWriter;
import java.util.List;
import java.util.ArrayList;


public class Importer {

	/*
	 * Import a set of files using the add method
	 */	
	public static String ImportFiles(String server, String collection, 
			String [] filenames, 
			String importMap, String abbrevMap,
			boolean overwrite) 
	{
		StringWriter log = new StringWriter();
		
		//encode use chunked encoding?
		boolean chunk = false;  // disabled, we aren't handling it properly
		
		ImportRESTful importer = new ImportRESTful(log,chunk);
		importer.setURI(server);
		importer.setCollection(collection);
		importer.setOverwrite(overwrite);
		
		if (importMap != null && importMap.length() > 0)
			importer.setImportMap(importMap);
		if (abbrevMap != null & abbrevMap.length() > 0)
			importer.setSpeciesAbbreviationMap(abbrevMap);
		
		// transform array of filenames to list of files 
		List<File> files = new ArrayList<File>();
		for (String name : filenames) {
			File f = new File(name);
			files.add(f);
		}
		
		importer.add(files, null);					
		return log.toString();
	}
	
	// not yet implemented - ODBC file
	public static String ImportODBCFiles(String server, String collection, 
			String [] filenames, boolean overwrite) {
		String result = null;
		
		return result;
	}
		
	// not yet implemented - ODBC network resource
	public static String ImportODBCNetworkResource(String server, String collection,
			String [] filenames, boolean overwrite) {
		String result = null;
		
		return result;
	}	
	
}
