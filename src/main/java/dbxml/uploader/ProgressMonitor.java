/**
 * https://blogs.oracle.com/PavelBucek/entry/using_containerlistener
 */


package dbxml.uploader;




import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;



import java.lang.Math;

import com.sun.jersey.api.client.filter.ContainerListener;


public class ProgressMonitor extends ContainerListener {
	//transmission states
	enum State{
		STARTED, //clicked Submit
		SENDING, //data being xferred
		ATTACHING, //ancillary handling
		STREAMING, //streaming data
		SENT, //file sent
		FINISHED, //received response
	}


	JProgressBar progress;
	

	ProgressMonitor(JProgressBar prog) {
		// invoke parent constructor
		super();
		progress = prog;
	}

	
    @Override
    public void onSent(long delta, long bytes) {
    	final int kBSent = (int) Math.ceil((bytes) / 1024.0);
    	progress.putClientProperty("state", State.SENDING);
    	progress.putClientProperty("kBSent", kBSent);
    	//currentBytes = bytes;
    	//System.out.println("onSent: dela: " + delta+ " bytes: "+ bytes);
    }

    @Override
    public void onReceiveStart(long totalBytes) {
        //System.out.println("onReceiveStart: " + totalBytes);
    }

    @Override
    public void onReceived(long delta, long bytes) {
    	
        //System.out.println("onReceived: delta: " + delta + " bytes: " + bytes);
    }

    @Override
    public void onFinish() {
    	//gets called once server has responded, not when finished sending
    	//System.out.println("onFinish: " );
    }
    
    
    
    

}

