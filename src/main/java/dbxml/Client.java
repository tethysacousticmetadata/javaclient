package dbxml;

import java.lang.String;
import java.net.URL;

import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.UniformInterfaceException;

public interface Client {

	/**
	 * getURL - Return the server URL
	 * @return URL string
	 */
	URL getURL();
	
	/***
	 * get- Get resource from specified server resource (assumes RESTful protocol)
	 * @param resourceId - Resource string relative to the web server, e.g. "Tethys/ping"
	 * @return String response
	 */
	public String get(String resourceId) 
			throws UniformInterfaceException, ClientHandlerException;
	
	/**
	 * ping - See if Tethys server is responding.
	 * @return
	 */
	boolean ping();
	
	/**
	 * String query(String XQuery)
	 * Run an XQuery and return it as a string
	 * 
	 * @param XQuery - Valid XQuery
	 */
	String query(String XQuery) throws Exception;
	
	/**
	 * Translate a JSON specification to an XQuery and
	 * run the query.
	 * @param json - See Tethys RESTFUL Web Services Interface for
	 *     format.
	 * @return query result
	 */
	String queryJSON(String json) throws Exception;

	/**
	 * Translate a JSON specification to an XQuery
	 * @param json - JSON specification, see Tethys RESTful Interface manual
	 *   for details.
	 * @param plan -
	 *    0 - execute query
	 *    1 - show query plan
	 *    2 - show XQuery
	 * @return - query result, plan, or XQuery
	 * @throws Exception Server errros due to bad queries, etc.
	 */
	String queryJSON(String json, int plan) throws Exception;
	
	/**
	 * Remove a document from a collection
	 * @param Collection  - Collection from which to delete
	 * @param DocId - document id.  This is the last part of the uri
	 * 
	 * e.g. uri:  dbxml:///Detections/SOCAL32M_mysticete
	 * delete_docId("Detections", "SOCAL32M_mysticete")
	 * 				
	 * @return operation status as XML string
	 */
	String removeDocument(String Collection, String DocId) throws Exception;
	
	/**
	 * Retrieve representation of a schema
	 * @param schema_root - Root element of the schema
	 * @return CSV string
	 */
	public String getSchema(String schema_root);
	
};
