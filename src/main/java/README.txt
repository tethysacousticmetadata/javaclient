JavaClient
Client for interfacing with Tethys server.

This client can be used directly with Java or via another language
such as Matlab.
